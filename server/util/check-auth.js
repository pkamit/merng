// const { AuthenticationError } = require('apollo-server');

// const jwt = require('jsonwebtoken');
// const { SECRET_KEY } = require('../config');
const admin = require('../firebase/index')


module.exports = async (context) => {
    // context = { ... headers }
    // const authHeader = context.req.headers.authorization;
    // if (authHeader) {
    //   // Bearer ....
    //   const token = authHeader.split('Bearer ')[1];
    //   if (token) {
    //     try {
    //       const user = jwt.verify(token, SECRET_KEY);
    //       return user;
    //     } catch (err) {
    //       throw new AuthenticationError('Invalid/Expired token');
    //     }
    //   }
    //   throw new Error("Authentication token must be 'Bearer [token]");
    // }
    // throw new Error('Authorization header must be provided');
    try {
      const currentUser = await admin.auth().verifyIdToken(context.req.headers.authtoken);
      console.log('CURRENT USER from authtoken', currentUser);
      return currentUser;
  } catch (error) {
      console.log('AUTH CHECK ERROR', error);
      throw new Error('Invalid or expired token');
  }
  };