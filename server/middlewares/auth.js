const admin = require('../firebase/index')
const mongoose = require("mongoose");
var User = mongoose.model('User')
exports.authCheck = async (req, res , next) =>{
    console.log(req.headers);
     try{
       const firebaseUser = await admin.auth().getUser(req.headers.authuid);
    //   console.log('Firebase user in auth check' , firebaseUser.toJSON());
       req.user =  firebaseUser.toJSON();
      
       //console.log(email);
        next();

    }catch(err){
      console.log(err);
        res.status(401).json({
            err: "Invalid or Expired token",
        });
      
    }
  //  next();
}

exports.adminCheck = async (req, res , next) =>{
  const {email} = req.user;
  const adminUser = await User.findOne({email}).exec()
  if(adminUser.email != 'amit.pandey812@gmail.com'){
    res.status(403).json({
      err: "Admin resource , Access denied"
    });
  }
  else{
    next(); 
  }

}

exports.authCheckToken = async (req, res , next) =>{
  try{
  console.log(req.headers);    

      const firebaseUser = await admin.auth().verifyIdToken(req.headers.authtoken);
    //  console.log("auth token",req.headers.authtoken);
     console.log('Firebase user in auth check' , firebaseUser);
      req.user = firebaseUser;
     
      next();

  }catch(err){
      res.status(401).json({
          err: "Invalid or Expired token",
      });

  }
//  next();
}
