const postsResolvers = require('./posts');
const usersResolvers = require('./users');
const commentsResolvers = require('./comments');


module.exports = {
    Post:{
     //   likeCount: (parent) => parent.likes.length,
       // commentCount: (parent) => parent.comments.length
     // likeCount: (parent) => parent.like.length
     index: (parent) => parent._id
    },

    Query: {
        ...postsResolvers.Query,
        ...usersResolvers.Query
    },
    Mutation: {
        ...usersResolvers.Mutation,
        ...postsResolvers.Mutation,
        ...commentsResolvers.Mutation

    },
    Subscription: {
        ...postsResolvers.Subscription
      }
}