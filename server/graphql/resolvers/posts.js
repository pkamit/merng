const { AuthenticationError, UserInputError } = require("apollo-server");

const Post = require("../../models/Post");
const User = require("../../models/User");
const checkAuth = require("../../util/check-auth");
module.exports = {
  Query: {
    async getPosts(_, { limit, index }) {
      try {
        const posts = await Post.find()
          .limit(parseInt(limit))
          .skip(parseInt(index))
          .sort({ _id: -1 });
        posts.pageInfo = {
          index: index,
        };
        return posts;
      } catch (err) {
        throw new Error(err);
      }
    },

    async getPost(_, { postId }) {
      try {
        //  const post = await Post.findById(postId);

        const post = await Post.findOne({ _id: postId })
          .populate("user")
          .populate("category")
          .populate("tag")
          .exec();
        if (post) {
          return post;
        } else {
          throw new Error("Post not found");
        }
      } catch (err) {
        throw new Error(err);
      }
    },

    //  async posts(_, { cursor, limit = 100 }){
    //   const whereOptions = cursor
    //     ?
    //    {_id : { $lt: cursor } }
    //       // {
    //       //   where: {
    //       //     _id: { $lt: cursor }
    //       //   }
    //       // }
    //     : {};

    //   // gives us an array of posts
    //   const posts = await Post.find({
    //     ...whereOptions})
    //     .limit(parseInt(limit))
    //     .sort({ createdAt: -1 });

    //   // put the data in the shape our client expects
    //   let edges = posts.map((post) => {
    //     return { node: post };
    //   });

    //   // return edges + page info
    //   return {
    //     edges: edges,
    //     pageInfo: {
    //       endCursor: posts[posts.length - 1].id
    //     }
    //   };
    // },

    async posts(_, { offset, limit }) {
      let totalCount = 0;

      // gives us an array of posts
      const posts = await Post.find({})
        .populate("user")
        .populate("category")
        .populate("tag")
        .limit(parseInt(limit))
        .skip(parseInt(offset))
        .sort({ _id: -1 });

      await Post.countDocuments({}, function (err, count) {
        if (err) {
          console.log(err);
        } else {
          totalCount = count;
        }
      });
      // console.log("Total Count :", totalCount);
      // put the data in the shape our client expects
      let edges = posts.map((post) => {
        return { node: post };
      });

      // return edges + page info
      return {
        edges: edges,
        pageInfo: {
          endCursor: posts[posts.length - 1].id,
        },
        totalCount: totalCount,
      };
    },
  },
  Mutation: {
    // async createPost(_, { body }, context) {
    //   const user = checkAuth(context);

    //   if (body.trim() === "") {
    //     throw new Error("Post body must not be empty");
    //   }

    //   const newPost = new Post({
    //     body,
    //     user: user.id,
    //     username: user.username,
    //     createdAt: new Date().toISOString(),
    //   });

    //   const post = await newPost.save();

    //   context.pubsub.publish("NEW_POST", {
    //     newPost: post,
    //   });

    //   return post;
    // },
    async createPost(_, { input }, context) {
      const user = await checkAuth(context);
      if (input.body.trim() === "") throw new Error("Body is required");
      if (input.title.trim() === "") throw new Error("Title is required");
      console.log("current user using creating post", user.email);
      const currentUserFromDb = await User.findOne({
        email: user.email,
      });
      console.log("current user using creating post", currentUserFromDb._id);
      console.log(input);
      let newPost = await new Post({
        ...input,
        user: currentUserFromDb._id,
      })
        .save()
        .then((post) =>
          post
            .populate("user")
            .populate("category")
            .populate("tag")
            .execPopulate()
        );

      return newPost;
    },
    async deletePost(_, { postId }, context) {
      const user = checkAuth(context);

      try {
        const post = await Post.findById(postId);
        if (user.username === post.username) {
          await post.delete();
          return "Post deleted successfully";
        } else {
          throw new AuthenticationError("Action not allowed");
        }
      } catch (err) {
        throw new Error(err);
      }
    },

    async publishPost(_, { postId, publish }, context) {
      const user = checkAuth(context);

      try {
        const post = await Post.findById(postId);
        // if (user.username === post.username) {
        //   await post.delete();
        //   return "Post deleted successfully";
        // } else {
        //   throw new AuthenticationError("Action not allowed");
        // }
        let updatedPost = await Post.findByIdAndUpdate(
          { _id: postId },
          { publish: publish },
          { new: true }
        )
          .exec()
          .then((post) =>
            post
              .populate("user")
              .populate("category")
              .populate("tag")
              .execPopulate()
          );
        return updatedPost;
      } catch (err) {
        throw new Error(err);
      }
    },

    async updatePost(_, { input }, context) {
      const user = checkAuth(context);
      console.log(input);
      try {
        if (input.body.trim() === "")
          throw new Error("Description is required!");
        if (input.title.trim() === "") throw new Error("Title is required!");
        //const post = await Post.findById(args.input.id);

        // let updatedPost = await Post.findOneAndUpdate(
        //   { _id: input.id },
        //   { title: input.title }
        // ).exec();
        // if (user.username === post.username) {
        //   await post.delete();
        //   return "Post deleted successfully";
        // } else {
        //   throw new AuthenticationError("Action not allowed");
        // }
        // var docs = {};
        // await Post.findByIdAndUpdate(
        //   {_id: '6051c90757b2815fdcc60a0e'},
        //   { title: "new test" },
        //   function (err, docs) {
        //     if (err) {
        //       console.log(err);
        //     } else {
        //       console.log("Updated User : ", docs);

        //     }
        //   }
        // );
        let updatedPost = await Post.findByIdAndUpdate(
          { _id: input._id },
         // { title: input.title, body: input.body , category: input.category , publish: input.publish , tag:input.tag},
         {...input},
          { new: true }
        )
          .exec()
          .then((post) =>
            post
              .populate("user")
              .populate("category")
              .populate("tag")
              .execPopulate()
          );
        return updatedPost;
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Subscription: {
    newPost: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator("NEW_POST"),
    },
  },
};
