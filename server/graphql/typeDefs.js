const { gql } = require("apollo-server");
module.exports =  gql`
  type Post {
    _id: ID!
    title: String
    body: String
    imageurl: String
    audiourl: String
    videourl: String
    youtubeurl: String
    createdAt: String
    username: String
    likes:  Int
    comments: Int
    shares: Int
    user: User
    index: String
    category: Category
    tag: Tag
    publish: Int
       

   
  }
  
  type PageInfo {
    endCursor: String! # will change to String later
  }




  type Comment{
    id: ID!
    createdAt: String!
    username: String!
    body: String!

  }

  type Likes{
    id: ID!
    createdAt: String!
    username: String!
    }

  type TodosResult {
    todos: [Post]
    totalCount: Int
  }

  type PostConnection {
    edges: [PostEdge!]!
    pageInfo: PageInfo!
    totalCount: Int
  }
  type PostEdge {
    node: Post!
  }
  type Query {

    posts(offset: Int, limit: Int): PostConnection!
    getPosts(limit: Int, index: Int): [Post]
    getPost(postId: String!): Post!
    getUser(username: String! ): Nuser!
    allTodos(
      first: Int,
      offset: Int
    ): TodosResult

  }



  type User{
    id: ID!
    email: String!
    token: String!
    username: String!
    profilename: String
    bio: String
    url: String
    createdAt: String!
  }

  type Nuser{
    id: ID!
    email: String!
    username: String!
    createdAt: String!
  }
  input RegisterInput{
    username: String!
    password: String
    confirmPassword: String
    email: String!
    profilename: String
    bio: String
    url: String
    

  }

  type Category{
    id: ID
    name: String
    slug: String
  }

  type Tag{
    id: ID
    name: String
    slug: String
  }

  input  PostUpdateInput{
    _id: ID!
    title: String
    body: String
    imageurl: String
    audiourl: String
    youtubeurl: String
    videourl: String
    likes:  Int
    comments: Int
    shares: Int
    category: String
    tag: String
    publish: Int
  }
  input  PostCreateInput{
    title: String
    body: String
    imageurl: String
    audiourl: String
    youtubeurl: String
    videourl: String
    likes:  Int
    comments: Int
    shares: Int
    category: String
    tag: String
    publish: Int
  }


  type Mutation{
    register(registerInput: RegisterInput): User!,
    login(username: String!, password: String!): User!
    createPost(input: PostCreateInput!): Post!
    updatePost(input: PostUpdateInput!): Post! 
    deletePost(postId: String!): String!
    createComment(postId: String!, body: String!): Post!
    deleteComment(postId: ID! , commentId: ID!): Post!
    likePost(postId: ID!): Post!
    publishPost(postId: ID! , publish: Int): Post!
  }


  type Subscription {
    newPost: Post!
  }
`;