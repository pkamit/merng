const express = require('express');

const router = express.Router();
//middleware
const {authCheck } = require('../middlewares/auth')
const { definedateTime } = require('../middlewares/datetime');

const multer = require('multer');
//var dateFile = Date.now();
// function changeExt(fileName, newExt) {
//   var pos = fileName.includes(".") ? fileName.lastIndexOf(".") : fileName.length
//   var fileRoot = fileName.substr(0, pos)
//   var output = `${fileRoot}.${newExt}`
//   return output
// }
var imgstorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images/')
  },
  filename: function (req, file, cb) {
   
    cb(null, req.datetime + '-' + file.originalname)
  }
})

var uploadimage = multer({ storage: imgstorage })
var videostorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/videos/')
  },
  filename: function (req, file, cb) {
    cb(null, req.datetime + '-' + file.originalname)
  }
})

var uploadvideo = multer({ storage: videostorage })

var audiostorage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log("audio");
    cb(null, './public/audios/')
  },
  filename: function (req, file, cb) {
 
    cb(null, req.datetime + '-' + file.originalname)
  }
})

const storagenew = multer.diskStorage({
 
  destination: (req, file, cb) => {
    console.log("amit");
    cb(null, "./public/images/")
  },
  filename: (req, file, cb) => {
    console.log("pandey");
    cb(null, req.datetime + "-" + file.originalname)
  },
})

const uploadStorage = multer({ storage: storagenew })


var uploadaudio = multer({ storage: audiostorage })

const {
  
    create,
    read,
    uploadimageCtrl,
    uploadvideoCtrl,
    uploadaudioCtrl,
    uploadimageCtrlapi
    
 
  } = require("../controllers/post");


router.post("/create-post", authCheck, create);
router.get("/get-posts", read);
router.post("/uploadimage" , definedateTime , uploadStorage.single('file') , uploadimageCtrl );
//router.post("/uploadimageapi" , definedateTime , uploadStorage.single('file'), uploadimageCtrlapi );
router.post("/uploadvideo" , definedateTime , uploadvideo.single('myFile') , uploadvideoCtrl );
router.post("/uploadaudio" , definedateTime , uploadaudio.single('file') , uploadaudioCtrl );



//router.post("/uploadyoutubevideo" , authCheck  ,  create );
//router.post("/uploadvideo" , uploadvideo.single('myFile') ,uploadvideoCtrl );

//router.get("/getuser/:username", read);
module.exports = router;