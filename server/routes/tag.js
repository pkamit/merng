const express = require("express");

const router = express.Router();

//middleware
const { adminCheck , authCheckToken } = require("../middlewares/auth");

const {
  create,
//   read,
//   update,
//   remove,
  list,
//   getSubs,
} = require("../controllers/tag");

//routes
router.post("/tag", authCheckToken , adminCheck,   create);
router.get("/tags", list);
// router.get("/category/:slug", read);
// router.put("/category/:slug", authCheck, adminCheck, update);
// router.delete("/category/:slug", authCheck, adminCheck, remove);
// router.get("/category/subs/:_id", getSubs);

module.exports = router;
