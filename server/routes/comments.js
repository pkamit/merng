const express = require('express');

const router = express.Router();
//middleware
const {authCheck } = require('../middlewares/auth')

const {
  
    create,
    read
   
 
  } = require("../controllers/comment");

router.post("/post-comments", authCheck, create);
router.post("/get-comments",  read);
//router.get("/getuser/:username", read);
module.exports = router;