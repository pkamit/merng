const express = require("express");
const {authCheck } = require('../middlewares/auth')
const router = express.Router();
const {
  
    read,
    readUsers,
    readCounts,
    getGuuid,
    setFollowers,
    deleteFollowers,
    checkFollower,
    readUserByGuuid,
    healthCheck

 
  } = require("../controllers/user");

router.get("/getuser/:username", read);
router.get("/healthcheck1", healthCheck);
router.post("/getallusers",   readUsers);
router.post("/getuserbyguuid",authCheck,   readUserByGuuid);
router.post("/getpostcount",   readCounts);
router.post("/getguuid", authCheck,   getGuuid);
router.post("/setfollowers", authCheck,   setFollowers);
router.delete("/deletefollowers", authCheck,  deleteFollowers);
router.post("/checkfollowers", authCheck,  checkFollower);
//router.get("/getuser/:username", read);
module.exports = router;
