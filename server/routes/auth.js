const express = require('express');

const router = express.Router()

//middleware
const {authCheck , authCheckToken ,adminCheck} = require('../middlewares/auth')

const { createOrUpdateUser , updateUser , currentUser } = require('../controllers/auth');

router.post('/create-or-update-user', authCheck, createOrUpdateUser);
router.post('/update-user', authCheck, updateUser);
router.post('/current-admin',authCheckToken,  adminCheck, currentUser);


module.exports = router;