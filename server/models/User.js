const {model , Schema} = require('mongoose');
const mongoose = require("mongoose");
const userSchema = new Schema({
    username: String,
    profilename : String,
    bio: String,
    url : String,
    email: {
        type: String,
        required: true,
        index: true,
    },
    password: String,
    createdAt : String,
    role: {
        type: String,
        default: "subscriber"
    },
    address: String,
    guuid: String,
    followers: {
        type: Number,
        default: 0
    },
    followings: {
        type: Number,
        default: 0
    }


}, {timestamps: true });

module.exports = mongoose.model('User' , userSchema);