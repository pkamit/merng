const {model , Schema} = require('mongoose');
const mongoose = require("mongoose");

const commentSchema = new Schema({
    content: { type: String, required: true },
    userid: { type: Schema.Types.ObjectId, ref: "User", required: true },
    postid: { type: Schema.Types.ObjectId, ref: "Post", required: true },
}, {timestamps: true });

module.exports = mongoose.model('Comment' , commentSchema);