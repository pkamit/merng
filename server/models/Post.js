const {model , Schema} = require('mongoose');
const mongoose = require("mongoose");
const {objectId} = mongoose.Schema
const postSchema = new Schema({
    username: String,
    title: String,
    body: String,
    createdAt: String,
    comment: [
        {
            body: String,
            username: String,
            createdAt: String

        }
    ],
    like:[
     
    ],
    user:{
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: "Category",
    },
    tag: {
        type: Schema.Types.ObjectId,
        ref: "Tag",
      },

    imageurl: String,
    videourl: String,
    audiourl: String,
    youtubeurl: String,
    likes: Number,
    comments: Number,
    shares: Number,
    publish: {
        type: Number,
        default: 0
    }
 




}, {timestamps: true });

module.exports = mongoose.model('Post' , postSchema);