
const mongoose = require('mongoose')
//const {objectId} = mongoose.Schema

const tagSchema = new mongoose.Schema({
    name : {
        type : String,
        trim : true,
        required : 'Name is required',
        minlength: [2, 'Too short']
    },
    slug: {
        type: String,
        unique: true,
        lowercase: true,
        index: true

    }
} , {timestamps : true});

module.exports = mongoose.model('Tag' , tagSchema);

