const { ApolloServer , PubSub } = require("apollo-server");
const mongoose = require("mongoose");
var path = require('path');
const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')
const {readdirSync} = require('fs');
const multer = require('multer');
fs = require('fs-extra')



const typeDefs = require("./graphql/typeDefs");
const resolvers = require("./graphql/resolvers");
const { MONGODB , PORT} = require("./config.js");
const app = express()

const pubsub = new PubSub();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req,  pubsub })

});


mongoose
  .connect(MONGODB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Mongodb connected");
    server.listen({ port: 5000 }).then((res) => {
      console.log(`Server running at 5000 ${res}`);
    });
  })
  .then((res) => {
    console.log(`Server running at 5000`);
  })
  .catch((err) => console.log(`Db connection error ${err}`));


//middlewares
app.use(morgan("dev"));
app.use(bodyParser.json({ limit : "2mb"}));
app.use(cors());
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/videos')));

//routes middleware
readdirSync('./routes').map((r) =>{
console.log(r);
app.use("/api",require('./routes/' + r))
}

);
// GET method route
// app.get('/api/getuser:username', function (req, res) {
//   res.send('hello world')
// })

const port = PORT || 8000;
app.listen(port, ()=> console.log('server is running'));