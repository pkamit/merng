const users = require("../graphql/resolvers/users");
const Post = require("../models/Post");
const User = require("../models/User");
const Like = require("../models/Like");
exports.create = async (req, res) => {
  try {
    const { uid } = req.user;
    console.log(uid);
    const authUser = await User.findOne({ guuid: req.body.userid }).exec();
    req.body.userid = authUser._id;
    //console.log(authUser._id);
    const userLike = await Like.findOne({
      postid: req.body.postid,
      userid: req.body.userid,
    }).exec();
    // console.log(userLike);
    var newLike = "";
    const post = await Post.findById(req.body.postid).exec();
    if (userLike) {
      Like.deleteOne({ _id: userLike._id }, function (err, results) {
        newLike = 0;
      });
      const userPost = await Post.findOne({ _id: req.body.postid }).exec();
      var likesNew = userPost.likes - 1;

      const updated = await Post.updateOne({ _id: req.body.postid }, [
        { $set: { likes: likesNew } },
      ]);

      let existingRatingObject = post.like.find(
        (ele) => ele.toString() === authUser.guuid.toString()
      );
      console.log("existing like", existingRatingObject);
      if (existingRatingObject) {
        let ratingAdded = await Post.findByIdAndUpdate(
          { _id: req.body.postid },
          {
            $pull: { like: authUser.guuid.toString() },
          }
        ).exec();
        console.log("like removed", ratingAdded);
      }
      console.log(updated, "Removed like");
    } else {
      newLike = await new Like(req.body).save();
      const userPost = await Post.findOne({ _id: req.body.postid }).exec();
      var likesNew = userPost.likes + 1;
      const updated = await Post.updateOne({ _id: req.body.postid }, [
        { $set: { likes: likesNew } },
      ]);
      console.log(updated, "Added like");
      newLike = 1;
    }
    let existingRatingObject = post.like.find(
      (ele) => ele.toString() === authUser.guuid.toString()
    );
    if (!existingRatingObject) {
      let ratingAdded = await Post.findByIdAndUpdate(
        { _id: req.body.postid },
        {
          $push: { like: authUser.guuid.toString() },
        }
      ).exec();
      console.log("like added", ratingAdded);
    }

    res.json(newLike);
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};
