const mongoose = require("mongoose");
var User = mongoose.model('User')
const jwt = require("jsonwebtoken");
require("../graphql/resolvers/users");
const { SECRET_KEY } = require("../config.js");

function generateRestToken(user) {

    return jwt.sign(
      {
        id: user.id,
        email: user.email,
        username: user.username
      },
      SECRET_KEY,
      { expiresIn: '1h' }
    );
  }

exports.createOrUpdateUser = async (req, res) =>{
    //
    const { email , displayName , photoURL , uid } = req.user;


    //const user = await User.findOneAndUpdate({email}, {userrname:email.split("@")[0], url: photoURL , profilename: displayName , guuid: uid} , {new: true})
    const user = await User.findOne({email});
    if(user){
        console.log("USER UPDATED", user);
        const token = generateRestToken(user);
        const { email ,  profilename , url , role }  = user
         
        res.json( {email ,  profilename, url  , token , role  });
      
    }else{
        const newUser = await new User({
            email,
            username : email.split("@")[0],
            profilename: displayName,
            url : photoURL,
            guuid: uid
        }).save();
        console.log("USER CREATED", newUser);
        const token = generateRestToken(newUser);
        const { email ,  profilename , url , role }  = newUser
        res.json( {email ,  profilename, url  , token , role });
       
    }

    
    }

    exports.updateUser = async (req, res) =>{
      //
      const { uid } = req.user;
  
  
      const user = await User.findOneAndUpdate({guuid: uid}, {profilename:req.body.profilename, url: req.body.imageurl , bio: req.body.bio } , {new: true})
  
      if(user){
          console.log("USER UPDATED", user);
          const token = generateRestToken(user);
          const { email ,  profilename , url , _id }  = user
           
          res.json( {id: _id });
        
      }
  
      
      }

      exports.currentUser = async (req, res) =>{
        //
        User.findOne({email:req.user.email}).exec((err, user)=> {
            if(err) throw new Error(err)
            res.json(user);
        });
    
        
        }

 

