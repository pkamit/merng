const users = require("../graphql/resolvers/users");
const User = require("../models/User");
const Post = require("../models/Post");
const Comment = require("../models/Comment");

exports.create = async (req, res) => {
  try {
    const { uid } = req.user;
    console.log(uid);
    const authUser = await User.findOne({ guuid: req.body.userid }).exec();
    req.body.userid = authUser._id;
    // const userLike = await new Like({
    //   postid: req.body.postid,
    //   userid: req.body.userid,
    // }).save();
    newLike = await new Comment(req.body).save();
    Comment.countDocuments(
      { postid: req.body.postid, userid: req.body.userid },
      function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(400)
            .send("wrong userid " + req.body.userid + " in user collection.");
        } else {
          console.log(result);
          Post.findByIdAndUpdate(
            { _id: ""+req.body.postid+"" },
            { comments: result },
            function (err, result) {
              if (err) {
                //res.send(err)
                console.log(err);
              } else {
                console.log("updated result " + result);
              }
            }
          );
          // console.log("Comments count" + updated);
          //   res.json(result);
        }
      }
    );
    res.json(newLike);
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};

exports.read = async (req, res) => {
  try {
    //  const { uid } = req.user;
    console.log(req.body);
    const commentLists = await Comment.find(
      { postid: req.body.postid },
      { _id: 0 }
    )
      .populate("userid", "_id profilename url")
      .sort({ createdAt: -1 })
      .exec();
    //req.body.userid = authUser._id;
    // const userLike = await new Like({
    //   postid: req.body.postid,
    //   userid: req.body.userid,
    // }).save();

    res.json(commentLists);
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};
