const users = require("../graphql/resolvers/users");
const Post = require("../models/Post");
const User = require("../models/User");


exports.uploadimageCtrl = async (req, res , next) => {
  try {
    const file = req.file
    //console.log(file);
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
  
    }
  //   var finalVideo = {
  //     contentType: req.file.mimetype,
  //     video:  dateFile + '-' + file.originalname
  //  };
    // db.collection('videocollection').insertOne(finalVideo, (err, result) => {
    // //	console.log(result)
  
    //   if (err) return console.log(err)
  
    //   console.log('saved to database')
    //  // res.redirect('/')
    
      
    // })
  
    res.json({ fileUrl: 'http://10.0.2.2:8000/images/' + req.datetime + '-' + file.originalname });
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};

exports.uploadimageCtrlapi = async (req, res , next) => {
  try {
    const file = req.file
   
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
  
    }
    res.json({ fileUrl: 'http://10.0.2.2:8000/images/' + req.datetime + '-' + file.originalname });
 
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
 
};


exports.uploadvideoCtrl = async (req, res , next) => {
  try {
    const file = req.file
    console.log(file);
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
  
    }
  //   var finalVideo = {
  //     contentType: req.file.mimetype,
  //     video:  dateFile + '-' + file.originalname
  //  };
    // db.collection('videocollection').insertOne(finalVideo, (err, result) => {
    // //	console.log(result)
  
    //   if (err) return console.log(err)
  
    //   console.log('saved to database')
    //  // res.redirect('/')
    
      
    // })
  
    res.json({ fileUrl: 'http://10.0.2.2:8000/videos/' + req.datetime + '-' + file.originalname });
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};

exports.uploadaudioCtrl = async (req, res , next) => {
  try {
    const file = req.file
    console.log(file);
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
  
    }
  //   var finalVideo = {
  //     contentType: req.file.mimetype,
  //     video:  dateFile + '-' + file.originalname
  //  };
    // db.collection('videocollection').insertOne(finalVideo, (err, result) => {
    // //	console.log(result)
  
    //   if (err) return console.log(err)
  
    //   console.log('saved to database')
    //  // res.redirect('/')
    
      
    // })
  
    res.json({ fileUrl: 'http://10.0.2.2:8000/audios/' + req.datetime + '-' + file.originalname });
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};




exports.create = async (req, res) => {
  try {
    const { uid } = req.user;
    const authUser = await User.findOne({ guuid: uid }).exec();
    req.body.user = authUser._id;
    console.log(req.body);

    const newPost = await new Post(req.body).save();
    
    res.json(newPost);
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};

exports.read = async (req, res) => {
  console.log(req.headers.authuid);
  const { page = 1, limit = 5 } = req.query;
  let posts = "";
  let count = 0;
  try {
    if(req.headers.authuid != "123" ){
       posts = await Post.find({ user: req.headers.authuid })
      .populate("user" , "_id profilename url")
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .sort({ _id: -1 })
      
      .exec();
    // get total documents in the Posts collection
     count = await Post.countDocuments({ user: req.headers.authuid });
    }
    else{
     posts = await Post.find()
      .populate("user" , "_id profilename url")
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .sort({ _id: -1 })
      
      .exec();
    // get total documents in the Posts collection
     count = await Post.countDocuments();
  //  const {profilename , url} = posts.user;
    }
      console.log(posts);
    res.json({
      posts,
      totalPages: Math.ceil(count / limit),
      page: parseInt(page),
    });
  } catch (err) {
    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};
