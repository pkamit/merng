const Post = require("../models/Post");
const User = require("../models/User");
const Followers = require("../models/Followers");
const Followings = require("../models/Followings");

const mongoose = require('mongoose');


exports.read = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.params.username }).exec();
    const { email, username, _id } = user;

    res.json({ email: email, username: username, id: _id });
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};
exports.readUsers = async (req, res) => {
  try {
    let user = "";
    if (req.body.userid) {
      user = await User.find({ _id: req.body.userid }).exec();
    } else {
      user = await User.find({}).exec();
    }
    console.log(req.body.userid);

    // let user = await User.find({}).exec();
    //const {email , username , _id , url} =  user;

    res.json(user);
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};

exports.readUserByGuuid = async (req, res) => {
  try {
    let user = "";
    if (req.body.userguuid) {
      user = await User.find({ guuid: req.body.userguuid }).exec();
    } 
    console.log(req.body.userguuid);

    // let user = await User.find({}).exec();
    //const {email , username , _id , url} =  user;

    res.json(user);
  } catch (err) {
    res.status(400).send("user reading fail by guuid");
  }
};

exports.readCounts = async (req, res) => {
  try {
    //  let count = 0;

    Post.count({ user: req.body.userid }, function (err, result) {
      if (err) {
        console.log(err);
        res
          .status(400)
          .send("wrong userid " + req.body.userid + " in user collection.");
      } else {
        console.log(req.body);
        console.log(result);
        res.json(result);
      }
    });
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};
exports.getGuuid = async (req, res) => {
  try {
    //  let count = 0;

    let user = "";
    if (req.body.userid) {
      user = await User.find({ _id: req.body.userid }).exec();
    }

    res.json(user);
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};
exports.setFollowers = async (req, res) => {
  try {
    //  let count = 0;

    const followerid_user = await User.find({
      guuid: req.body.followerid,
    }).exec();
    const followingid_user = await User.find({
      guuid: req.body.followingid,
    }).exec();
    console.log(followerid_user[0].id);
    console.log(followingid_user[0].id);
    let followers = new Followers({
      _f: followerid_user[0].id,
      _t: followingid_user[0].id,
    });
    followers
      .save()
      .then((item) => {
        console.log("followers saved to database");
      })
      .catch((err) => {
        console.log("unable to save to database");
      });
    let followings = new Followings({
      _f: followingid_user[0].id,
      _t: followerid_user[0].id,
    });
    followings
      .save()
      .then((item) => {
        console.log("followings saved to database");
      })
      .catch((err) => {
        console.log("unable to save to database");
      });
    const followersPost = await User.findOne({
      _id: followingid_user[0].id,
    }).exec();
    var followerssNew = followersPost.followers + 1;
    // const updated = await Post.updateOne({ _id: req.body.postid }, [
    //   { $set: { likes: likesNew } },
    // ]);
    const updated = await User.findOneAndUpdate(
      { _id: followingid_user[0].id },
      { followers: followerssNew }
    ).exec();

    const followingsPost = await User.findOne({
      _id: followerid_user[0].id,
    }).exec();
    var followingsNew = followingsPost.followings + 1;
    const updated1 = await User.findOneAndUpdate(
      { _id: followerid_user[0].id },
      { followings: followingsNew }
    ).exec();

    res.json("this is test");
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};

exports.deleteFollowers = async (req, res) => {
  try {
    const followerid_user = await User.find({
      guuid: req.headers.followerid,
    }).exec();
    const followingid_user = await User.find({
      guuid: req.headers.followingid,
    }).exec();
    const followingsDeleted = await Followings.findOneAndRemove({
      _f: followingid_user[0].id,
      _t: followerid_user[0].id,
    }).exec();

    const followersDeleted = await Followers.findOneAndRemove({
      _f: followerid_user[0].id,
      _t: followingid_user[0].id,
    }).exec();

    const followersPost = await User.findOne({
      _id: followingid_user[0].id,
    }).exec();
    var followerssNew =  followersPost.followers > 0 ? followersPost.followers - 1 : 0;
    // const updated = await Post.updateOne({ _id: req.body.postid }, [
    //   { $set: { likes: likesNew } },
    // ]);
    const updated = await User.findOneAndUpdate(
      { _id: followingid_user[0].id },
      { followers: followerssNew }
    ).exec();

    const followingsPost = await User.findOne({
      _id: followerid_user[0].id,
    }).exec();
    var followingsNew = followingsPost.followings > 0 ? followingsPost.followings - 1 : 0;
    const updated1 = await User.findOneAndUpdate(
      { _id: followerid_user[0].id },
      { followings: followingsNew }
    ).exec();


    res.json("Followers and followings deleted");
  } catch (err) {
    console.log(err);
    return res.staus(400).send("Followings delete failed");
  }
};

exports.checkFollower = async (req, res) => {
  try {
    //  let count = 0;
    const followerid_user = await User.find({
      guuid: req.body.followerid,
    }).exec();
    // const followingid_user = await User.find({
    //   _id: req.body.followingid,
    // }).exec();
   
console.log(req.body);
   const  user = await Followers.find({
      _f: followerid_user[0].id,
      _t: req.body.followingid,
    }).exec();

   
     if( user.length > 0 ){
       console.log(user);
      res.json(true);
     }
     else{
      res.json(false);
     }
   
  } catch (err) {
    res.status(400).send("user reading fail in checking followers");
  }
};



exports.healthCheck = async (req, res) => {
  try {

    require('../../server/'); 
    var mongoose = require('mongoose');
    console.log(mongoose.connection.readyState);
   // const user = await User.find({}).exec();
   

    res.json({healthy: mongoose.connection.readyState==1 ? "up" : "down"});
  } catch (err) {
    res.status(400).send("user reading fail");
  }
};