const Tag = require('../models/Tags')
const slugify = require("slugify")

exports.create = async (req , res) => {
    try{
        const {name , slug} = req.body.category;
        console.log(name + slug);
      //  const category = await new Category({name , slug: slugify(name)}).save();
        res.json(await new Tag({ name , slug: slugify(slug)}).save());
    }
    catch (err){
        console.log(err)
        res.status(400).send('Create tag failed')
    }
}

// exports.update = async(req , res) => {
//     const {name} = req.body;
//     try{
//     const category = await Category.findOneAndUpdate({slug:req.params.slug}, {name , slug:slugify(name)} , {new: true});
//     res.json(category);
//     }
//     catch(err){
//         res.status(400).send("Updating category fail");
//     }
// }

// exports.read = async(req , res) => {
   
//     try{
//         let category = await Category.findOne({slug: slugify(req.params.slug)}).exec();
//         res.json(category);
//     }catch(err){
//         res.status(400).send("Category reading fail");
//     }
    
// }

// exports.remove = async(req , res) => {
//     try{
//         const deleted =  await Category.findOneAndDelete({slug: slugify(req.params.slug)});
//        // res.status(200).send(`category ${req.params.slug} has been deleted`);
//        res.json(deleted)
//     }catch(err){
//         res.status(400).send("deleted action fail");
//     }
    
// }

exports.list = async (req , res) => {
        res.json(await Tag.find({}).sort({ createdAt : -1}).exec());
}

// exports.getSubs = (req, res) => {
//     Sub.find({ parent: req.params._id }).exec((err, subs) => {
//       if (err) console.log(err);
//       res.json(subs);
//     });
//   };