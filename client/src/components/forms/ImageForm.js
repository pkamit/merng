import React, { useState, useContext, useEffect, fragment } from "react";
import Button from "react-bootstrap/Button";
import { getCategories } from "../../functions/category";

const ImageForm = ({
  handleSubmit,
  handleChange,
  title,
  body,
  handleOnUploadFile,
  handleCatagoryChange,
  handleTagChange,
  values,
  setValues,
  tags,
  selectedCategory,
  selectedTag,
  rememberMeEl
}) => {
  const { categories, category, tag , checked } = values;

  return (
    <form onSubmit={handleSubmit} encType="multipart/form-data">
      <hr></hr>

      <div className="form-group">
        <label htmlFor="title" className="font-weight-bold">
          Title
        </label>
        <input
          type="text"
          value={title}
          onChange={handleChange}
          name="title"
          className="form-control"

          //  placeholder="Write something cool"

          //  disabled={loading}
        />
      </div>
      <div className="form-group">
        <label htmlFor="body" className="font-weight-bold">
          Body
        </label>
        <textarea
          value={body}
          onChange={handleChange}
          name="body"
          rows="10"
          className="md-textarea form-control"
          //  placeholder="Write something cool"
          maxLength="150"
          //  disabled={loading}
        ></textarea>
      </div>
      {/* <input type="file" name="file" onChange={filehandleChange}/> */}
      <div className="form-group">
        <label htmlFor="image" className="font-weight-bold">
          Image
        </label>
        <div>
          <input type="file" name="file" onChange={handleOnUploadFile} />
        </div>
      </div>
      <div className="form-group">
        <label className="font-weight-bold">Category</label>
        {categories != null ? (
          <select
            name="category"
            className="form-control"
            onChange={handleCatagoryChange}
            value={selectedCategory ? selectedCategory : category._id}
          >
            <option>Please select</option>
            {categories.length > 0 &&
              categories.map((cat) => (
                <option key={cat._id} value={cat._id}>
                  {cat.name}
                </option>
              ))}
          </select>
        ) : (
          ""
        )}
      </div>
      <div className="form-group">
        <label className="font-weight-bold">Tag</label>
        {tags != null ? (
          <select
            name="tag"
            className="form-control"
            onChange={handleTagChange}
            value={selectedTag ? selectedTag : tag._id}
          >
            <option>Please select</option>
            {tags.length > 0 &&
              tags.map((t) => (
                <option key={t._id} value={t._id}>
                  {t.name}
                </option>
              ))}
          </select>
        ) : (
          ""
        )}
      </div>
      <div className="form-group">
        <label className="font-weight-bold">Publish</label>
        <br/>
    
        <input type="checkbox" ref={rememberMeEl} />
      </div>
      
      <div className="form-group">
        {/* <button className="btn btn-primary" type="submit" >
          Post
        </button> */}
        <Button variant="outline-primary" type="submit">
          Post
        </Button>
      </div>
    </form>
  );
};
export default ImageForm;
