import React, { useState, useContext, useEffect, fragment } from "react";
import Button from "react-bootstrap/Button";
import { getCategories } from "../../functions/category";
import Image from "react-bootstrap/Image";
import Player from "../../pages/Player";

const UpdateForm = ({
  handleSubmit,
  handleChange,
  title,
  body,
  handleOnUploadFile,
  handleCatagoryChange,
  handleOnAudioUploadFile,
  handleTagChange,
  values,
  setValues,
  tags,
  selectedCategory,
  selectedTag,
  rememberMeEl,
}) => {
  const {
    categories,
    category,
    tag,
    checked,
    imageurl,
    category_id,
    tag_id,
    publish,
    audiofile
  } = values;
  if (publish) rememberMeEl.current.checked = publish;
  return (
    <form onSubmit={handleSubmit} encType="multipart/form-data">
      {JSON.stringify(audiofile)}
      <hr></hr>

      <div className="form-group">
        <label htmlFor="title" className="font-weight-bold">
          Title
        </label>
        <input
          type="text"
          value={title}
          onChange={handleChange}
          name="title"
          className="form-control"

          //  placeholder="Write something cool"

          //  disabled={loading}
        />
      </div>
      <div className="form-group">
        <label htmlFor="body" className="font-weight-bold">
          Body
        </label>
        <textarea
          value={body}
          onChange={handleChange}
          name="body"
          rows="10"
          className="md-textarea form-control"
          //  placeholder="Write something cool"
          maxLength="150"
          //  disabled={loading}
        ></textarea>
      </div>
      {/* <input type="file" name="file" onChange={filehandleChange}/> */}
      <div className="form-group">
        <label htmlFor="image" className="font-weight-bold">
          Image
        </label>
        <div>
          <input type="file" name="file" onChange={handleOnUploadFile} />
          <br></br>
          {imageurl != null && imageurl != "" ? (
            <label htmlFor="image" className="font-weight-bold">
              Image preview
            </label>
          ) : (
            ""
          )}
          <br></br>
          {imageurl != null && imageurl != "" ? (
            <img
              src={imageurl}
              className="thumbnail "
              height="100"
              width="100"
            />
          ) : (
            ""
          )}
        </div>
        <hr></hr>
        <div className="form-group">
          <label htmlFor="image" className="font-weight-bold">
            Audio File
          </label>
          <div>
            <input
              type="file"
              name="audiofile"
              onChange={handleOnAudioUploadFile}
              accept=".mp3,audio/*"
            />
          </div>
        </div>
      </div>

      {audiofile != "" && audiofile != null ? (
        <div className="form-group">
          <label className="font-weight-bold">Audio link</label>
          <Player url={audiofile} />
        </div>
      ) : (
        ""
      )}
      <div className="form-group">
        <label className="font-weight-bold">Category</label>
        {categories != null ? (
          <select
            name="category"
            className="form-control"
            onChange={handleCatagoryChange}
            value={selectedCategory ? selectedCategory : category}
          >
            <option>Please select</option>
            {categories.length > 0 &&
              categories.map((cat) => (
                <option key={cat._id} value={cat._id}>
                  {cat.name}
                </option>
              ))}
          </select>
        ) : (
          ""
        )}
      </div>
      <div className="form-group">
        <label className="font-weight-bold">Tag</label>
        {tags != null ? (
          <select
            name="tag"
            className="form-control"
            onChange={handleTagChange}
            value={selectedTag ? selectedTag : tag}
          >
            <option>Please select</option>
            {tags.length > 0 &&
              tags.map((t) => (
                <option key={t._id} value={t._id}>
                  {t.name}
                </option>
              ))}
          </select>
        ) : (
          ""
        )}
      </div>
      <div className="form-group">
        <label className="font-weight-bold">Publish</label>
        <br />

        <input type="checkbox" ref={rememberMeEl} />
      </div>

      <div className="form-group">
        {/* <button className="btn btn-primary" type="submit" >
          Post
        </button> */}
        <Button variant="outline-primary" type="submit">
          Post
        </Button>
      </div>
    </form>
  );
};
export default UpdateForm;
