import React from 'react'
const CategoryForm = ({handleSubmit, name , setName , slug, setSlug}) =>(
    <form onSubmit = { handleSubmit}>
    <div className="form-group">
        <label> Name </label>
        <br/>
        <input  type="text" className="form-control" value={name}
        onChange={(e)=> setName(e.target.value)} 
        autoFocus
        required
        />
                <br/>
          <label> Slug </label>
        <br/>
        <input  type="text" className="form-control" value={slug}
        onChange={(e)=> setSlug(e.target.value)} 
        autoFocus
        required
        />
        <br/>
        <button className="btn btn-outline-primary"> Save</button>

    </div>
    </form>
);
export default CategoryForm;