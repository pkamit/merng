import React , {useEffect, useState} from 'react'
import {Route , Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import LoadingToredirect from './LoadingToredirect'
import {currentAdmin} from '../../functions/auth'


const AdminRoute = ({ children, ...rest }) =>{
    var retrievedObject = localStorage.getItem('addtoken');
    let user = JSON.parse(retrievedObject);
    const [ok, setOk] = useState(false);
    useEffect (() =>{
        if(user && user.token){
            currentAdmin(user.token).
            then(res =>{
                console.log('CUREENT ADMIN RES' , res);
                setOk(true);
            })
            .catch(err => {
                console.log('ERROR in CUREENT ADMIN RES' , err);
                setOk(false);
            })

        }
    },[user]);
   // console.log("USER",user.token);
    return ok ? (
        <Route {...rest} render={()=> children}/>
       
    ): (
        <LoadingToredirect></LoadingToredirect>
        
    )
}

export default AdminRoute;