import React from 'react'
import {Route , Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import LoadingToredirect from './LoadingToredirect'


const UserRoute = ({ children, ...rest }) =>{
    const {user} = useSelector((state) => ({...state}) );
   // console.log("USER",user.token);
    return user && user.token ? (
        <Route {...rest} render={()=> children}/>
       
    ): (
        <LoadingToredirect></LoadingToredirect>
        
    )
}

export default UserRoute;