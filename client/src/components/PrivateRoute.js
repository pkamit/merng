import React, { useContext, useState, useEffect } from 'react';
import { Route, Link } from 'react-router-dom';
import { AuthContext } from '../context/authContext';

const PrivateRoute = ({ children, ...rest }) => {
    const { state } = useContext(AuthContext);
    const [user, setUser] = useState(false);

    useEffect(() => {
        if (state.user) {
            setUser(true);
        }
    }, [state.user]);

    const navLinks = () => (
        <nav>
            <ul className="nav flex-column">
                {/* <li className="nav-item">
                    <Link className="nav-link" to="/profile">
                        Profile
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/password/update">
                        Password
                    </Link>
                </li> */}
                <li className="nav-item">
                    <Link className="nav-link" to="/post/create">
                        Create Post
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/category/create">
                        Create Category
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/tag/create">
                        Create Tag
                    </Link>
                </li>
                
            </ul>
        </nav>
    );

    const renderContent = () => (
        <div className="container-fluid pt-4">
            <div className="row">
                <div className="col-md-2">{navLinks()}</div>
                <div className="col-md-10">
                    <Route {...rest} />
                </div>
            </div>
        </div>
    );

    return user ? renderContent() : <h4>Loading...</h4>;
};

export default PrivateRoute;
