import React, { useState , useEffect } from "react";
import { Menu, Badge } from "antd";
import {
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
  LoginOutlined,
  UserAddOutlined,
  DashboardOutlined,
  UserOutlined,
  ShoppingOutlined,
  LogoutOutlined,
  ShoppingCartOutlined,
} from "@ant-design/icons";

import firebase from "firebase/app";
import "firebase/auth";

import { useDispatch, useSelector } from "react-redux";

import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
//import { LOGOUT } from "../../reducers/constant";
//import Search from "../forms/Search";

const { SubMenu, Item } = Menu;

const Header = () => {
  //let dispatch = useDispatch();
 // let { user, cart } = useSelector((state) => ({ ...state }));
  var retrievedObject = localStorage.getItem('addtoken');
  let user = JSON.parse(retrievedObject);
  let history = useHistory();
  const [current, setCurrent] = useState("home");
  //console.log("Cart" , cart);
  const handleClick = (e) => {
    setCurrent(e.key);
  };

 
  const logout = () => {
    firebase.auth().signOut();
    // dispatch({
    //   type: LOGOUT,
    //   payload: null,
    // });
    localStorage.removeItem('addtoken');
  //  window.location.reload();
    history.push("/login");
  };

  return (
  
    <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
      <Item key="home" icon={<AppstoreOutlined />}>
        <Link to="/">Home</Link>
      </Item>
   
      <Item key="shop" icon={<ShoppingOutlined />}>
        <Link to="/shop">Shop</Link>
      </Item>
   
      {/* <Item key="cart" icon={<ShoppingCartOutlined />}>
        <Link to="/cart">
          <Badge count={cart.length} offset={[9, 0]}>
            Cart
          </Badge>
        </Link>
      </Item> */}

      {localStorage.getItem('addtoken')==null && (
        <Item key="register" icon={<UserAddOutlined />} className="float-right">
          <Link to="register">Register</Link>
        </Item>
      )}
      {localStorage.getItem('addtoken')==null && (
        <Item key="login" icon={<LoginOutlined />} className="float-right">
          <Link to="login">Login</Link>
        </Item>
      )}

      {localStorage.getItem('addtoken')!=null && (
        <SubMenu
          key="Dashbored"
          icon={<DashboardOutlined />}
          title={user.email && "Hello " + user.email.split("@")[0]}
          className="float-right"
        >
          {user && user.role === "subscriber" && (
            <Item>
              <Link to="/user/history"> Dashboard </Link>
            </Item>
          )}
          {user && (
            <Item>
              <Link to="/admin/dashboard"> Dashboard </Link>
            </Item>
          )}
          <Item icon={<LogoutOutlined />} onClick={logout}>
            Logout
          </Item>
        </SubMenu>
      )}
      <span className="float-right p-1">
      
      </span>
    </Menu>
  );
};

export default Header;
