import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {ApolloClient} from '@apollo/client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';
import { setContext } from 'apollo-link-context';

import {createStore} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import  rootReducer  from "./reducers";
import { offsetLimitPagination } from "@apollo/client/utilities";

const store = createStore(rootReducer, composeWithDevTools())
const httpLink = createHttpLink({
    uri: 'http://localhost:5000'
  });
  
  const authLink = setContext(() => {
    const token = localStorage.getItem('addtoken');
    return {
      headers: {
        Authorization: token ? `Bearer ${token}` : ''
      }
    };
  });
  
  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache({

    }),
  });
  
  export default (
    <ApolloProvider client={client} >
      <App />
    </ApolloProvider>
  );