// import React from 'react';
// import {BrowserRouter as Router, Route , Switch } from 'react-router-dom';
// import { Container } from 'semantic-ui-react';
// import {ToastContainer} from 'react-toastify'

// import './App.css';
// import 'semantic-ui-css/semantic.min.css';
// import 'antd/dist/antd.css';

// import MenuBar from './components/MenuBar';
// import Home from './pages/Home';
// import Post from './pages/Post';
// import Login from './pages/Login';
// import Register from './pages/Register';
// import GetUsers from './components/GetUsers';
// import Header from './components/nav/Headers';
// import AdminRoute from './components/routes/AdminRoute'
// import AdminDashboard from './pages/admin/AdminDashboard'


// function App() {
//   let user = null;
//   var retrievedObject = localStorage.getItem('addtoken');
//   user = JSON.parse(retrievedObject);
//   if(user){
//       console.log(user.email+ "Header page");
//      // window.location.reload(false);
//   }
//   return (
//     <>
//       <Router>
//      <Header />
//      <ToastContainer></ToastContainer>
//    <Switch>
  

//           <Route exact path="/" component={Login} />
//           <Route exact path="/login" component={Login} />
//           <Route exact path="/register" component={Register} />
//           <AdminRoute exact path="/admin/dashboard" component={AdminDashboard}/>
//           <AdminRoute exact path="/admin/post/:id" component={Post}/>
//           {/* <Route exact path="/post/:id" component={Post} /> */}
//         </Switch>
//         </Router>
//       </>
//   );
// }

// export default App;
// import React , {useContext, useState} from 'react';
// import ApolloClient from 'apollo-boost'
// import {gql} from 'apollo-boost'
// import {AuthContext} from './context/authContext'
// const client = new ApolloClient({
//    uri: process.env.REACT_APP_GRAPHQL_ENDPOINT

//   });


// const App = () =>{
//   const [allpost, setAllPosts] = useState({})
//   const {state, dispatch} = useContext(AuthContext);

//   client.query({
//     query : gql `
//       query GetPosts($offset: Int) {
//     posts(limit: 10, offset: $offset) {
//       edges {
//         node {
//           id
//           title
//           body
//         }
//       }
//       pageInfo {
//         endCursor
//       }
//       totalCount
//     }
//   }
//     `
//   },
//   {  variables: {
      
//     offset: 0,
//     limit: 10
//   }}
//   ).then(result => setAllPosts(result.data.posts.edges));

//   return (<p>
// {JSON.stringify(state.user)}
//     {/* {allpost.map((p) => (<div key={p.node.key}>{p.node.title}</div>))} */}
//     hello
//   </p>);

// } 

// export default App;


import React, { useState , useContext } from 'react';
import { Switch, Route } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { gql } from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import './App.css';
// import components
import Nav from './components/Nav';
import Home from './pages/Home';
import Register from './pages/auth/Register';
import Login from './pages/Login';
import Post from './pages/post/Post';
import PostUpdatNew from './pages/post/PostUpdateNew';
import PrivateRoute from './components/PrivateRoute';
import Profile from './pages/auth/Profile';
import { AuthContext } from './context/authContext';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CategoryCreate from './pages/admin/category/CategoryCreate';
import CreateTag from './pages/admin/tag/CreateTag';
import PostDetail from './pages/post/PostDetail';

const App = () => {
    const { state } = useContext(AuthContext);
    const { user } = state;
    const client = new ApolloClient({

        uri: process.env.REACT_APP_GRAPHQL_ENDPOINT,
        request: (operation) => {
            operation.setContext({
                headers: {
                    authtoken: user ? user.token : ''
                }
            });
        }
    });
    return (
        <ApolloProvider client={client}>
            <Nav />
            <ToastContainer />
            <Switch>
                <PrivateRoute exact path="/" component={Home} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                {/* <PrivateRoute exact path="/profile" component={Profile} /> */}
                <PrivateRoute exact path="/post/create" component={Post} />
                <PrivateRoute exact path="/category/create" component={CategoryCreate} />
                <PrivateRoute exact path="/tag/create" component={CreateTag} />
                <PrivateRoute exact path="/post/update/:postid" component={PostUpdatNew} />
                <PrivateRoute exact path="/post/detail/:postid" component={PostDetail} />
                <PrivateRoute exact path="/profile" component={Profile} />
            </Switch>
        </ApolloProvider>
    );
};

export default App;
