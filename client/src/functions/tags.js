import axios from 'axios';

export const getTags = async() =>
     await axios.get(`${process.env.REACT_APP_API}/tags`); 
     
    
    




     

export const createTag = async(category ,  authtoken) =>
     await axios.post(`${process.env.REACT_APP_API}/tag`,
     {category},
     
     {
         headers: {
             authtoken
         }
     });    
     
