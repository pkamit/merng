//import gql from 'graphql-tag';
//import {gql} from '@apollo/client';
//import gql from "graphql-tag";
import { gql } from 'apollo-boost';
import { POST_DATA } from '../util/fragments';

export const FETCH_POSTS_QUERY = gql`
  query GetPosts($offset: Int) {
    posts(limit: 10, offset: $offset) {
      edges {
        node {
       ...postData
      }
      }
      pageInfo {
        endCursor
      }
      totalCount
    }
  }
  ${POST_DATA}
`;

export const FETCH_PER_POST_QUERY = gql`
  query getPost($postId: String!){
    getPost(postId: $postId) {
      ...postData
    }
  }
  ${POST_DATA}
`;

// export const FETCH_POSTS_QUERY = gql`
// query{
//   getPosts{
//         id
//       body
//       createdAt
//       username

//   }
// }
// `
