import { gql } from 'apollo-boost';
import { POST_DATA } from '../util/fragments';

export const POST_UPDATE = gql`
    mutation updatePost($input: PostUpdateInput!) {
        updatePost(input: $input) {
            ...postData
        }
    }
    ${POST_DATA}
`;

export const POST_CREATE = gql`
    mutation createPost($input: PostCreateInput!) {
        createPost(input: $input) {
            ...postData
        }
    }
    ${POST_DATA}
`;
export const POST_PUBLISH = gql`
    mutation publishPost($postId: ID! , $publish: Int) {
        publishPost(postId: $postId , publish: $publish  ) {
            ...postData
        }
    }
    ${POST_DATA}
`;