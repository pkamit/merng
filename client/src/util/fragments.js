import { gql } from 'apollo-boost';

export const POST_DATA = gql`
    fragment postData on Post {

      _id
      title
      body
      imageurl
      audiourl
      videourl
      youtubeurl
      likes
      comments
      category{
          id
          name
          slug
      }
      tag{
          id
          name
          slug
      }
      user{
            id
            email
            profilename
            url
        }
        publish
        createdAt
  
    }
`;