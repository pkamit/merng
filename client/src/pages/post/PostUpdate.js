import React, { useState, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import { FETCH_PER_POST_QUERY } from "../../util/graphql";
import { POST_UPDATE } from "../../util/mutation";
import ReactPlayer from "react-player";
import { useParams } from "react-router-dom";
import MultiPlayer from "../Multiplayer";
import Player from "../Player";
import { Image } from "antd";
const PostUpdate = () => {
  const [values, setValues] = useState({
    title: "",
    body: "",
    imageurl: "",
    audiourl: "",
    videourl: "",
    youtubeurl: "",
    profilename: "",
    useremail: "",
  });
  const [getSinglePost, { data: singlePost }] = useLazyQuery(
    FETCH_PER_POST_QUERY
  );
  const [postUpdate] = useMutation(POST_UPDATE);

  const [loading, setLoading] = useState(false);
  // router
  const { postid } = useParams();
  // destructure
  const { title, body, imageurl, audiourl, videourl, youtubeurl , profilename , useremail } = values;

  useMemo(() => {
    if (singlePost) {
      setValues({
        ...values,
        _id: singlePost.getPost._id,
        title: singlePost.getPost.title,
        body: singlePost.getPost.body,
        imageurl:
          singlePost.getPost.imageurl != null
            ? singlePost.getPost.imageurl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.imageurl,
        audiourl:
          singlePost.getPost.audiourl != null
            ? singlePost.getPost.audiourl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.audiourl,
        videourl: singlePost.getPost.videourl,
        youtubeurl: singlePost.getPost.youtubeurl,
        profilename: singlePost.getPost.user.profilename,
        useremail: singlePost.getPost.user.email,

      });
    }
  }, [singlePost]);

  useEffect(() => {
    console.log(postid);
    getSinglePost({ variables: { postId: postid } });
  }, []);

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    console.log(values);
    postUpdate({ variables: { input: values } });
    setLoading(false);
    toast.success("Post Updated");
  };

  const updateForm = () => (
    <form onSubmit={handleSubmit}>
           <p className="text-sm-left text-muted">
        {profilename}
        <div className="font-italic">
        {useremail}
        </div>
        </p>
     
         <hr></hr>
      <button
        className="btn btn-primary float-right"
        type="submit"
        disabled={loading || !body}
      >
        Post
      </button>
      <div class="form-check float-right pt-2">
        <input
          type="checkbox"
          className="form-check-input"
          id="publishPost"
          checked
        />
        <label className="form-check-label" for="publishPost">
          Publish
        </label>
   
      </div>
      
      <div className="form-group">
        <label className="label">
          <span className="field font-weight-bold"> Title:</span>
        </label>
        <textarea
          value={title}
          onChange={handleChange}
          name="title"
          rows="5"
          className="md-textarea form-control"
          placeholder="Write Title"
          maxLength="150"
          disabled={loading}
        ></textarea>
        <label className="label">
          <span className="field font-weight-bold"> Description:</span>
        </label>
        <textarea
          value={body}
          onChange={handleChange}
          name="body"
          rows="5"
          className="md-textarea form-control"
          placeholder="Write description"
          maxLength="150"
          disabled={loading}
        ></textarea>
      </div>
      {imageurl != null ? (
        <div className="form-group">
          <label for="youtubeLink" className="font-weight-bold">
            Image preview
          </label>
          <div>
            <Image width={250} src={imageurl} />
          </div>
        </div>
      ) : (
        ""
      )}
      {youtubeurl != null ? (
        <div className="form-group">
          <label for="youtubeLink" className="font-weight-bold">
            Youtube link
          </label>
          <input
            type="text"
            className="form-control"
            id="youtubeLink"
            value={youtubeurl}
          />
        </div>
      ) : (
        ""
      )}
      {youtubeurl != null ? (
        <div className="form-group">
          <label for="youtubeLink" className="font-weight-bold">
            Youtube link preview
          </label>
          <div className="player-wrapper1">
            <ReactPlayer
              url={youtubeurl}
              className="react-player"
              playing
              width="100%"
              height="100%"
            />
          </div>
        </div>
      ) : (
        ""
      )}
      {audiourl != null ? (

        <div className="form-group">
          {JSON.stringify(audiourl)}
          <label className="font-weight-bold">Audio link</label>
          <Player url={audiourl} />
        </div>
      ) : (
        ""
      )}
      {/* <div class="form-group">
        <label for="exampleInputEmail1">Audio link</label>
        <input type="text" className="form-control" id="audioLink" value="" />
      </div> */}

      <hr></hr>
    </form>
  );

  return (
    <div className="container">
      {loading ? (
        <h4 className="text-danger">Loading...</h4>
      ) : (
        <h4> Update post </h4>
      )}
   
      {updateForm()}
    </div>
  );
};

export default PostUpdate;
