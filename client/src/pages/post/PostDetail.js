import React, { useState, useMemo, useEffect } from "react";
import { toast } from "react-toastify";
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import { FETCH_PER_POST_QUERY } from "../../util/graphql";
import { POST_UPDATE } from "../../util/mutation";
import ReactPlayer from "react-player";
import { useParams } from "react-router-dom";
import MultiPlayer from "../Multiplayer";
import Player from "../Player";
import { Image } from "antd";
import "./post.css";
const PostDetail = () => {
  const [values, setValues] = useState({
    title: "",
    body: "",
    imageurl: "",
    audiourl: "",
    videourl: "",
    youtubeurl: "",
    profilename: "",
    useremail: "",
    createdAt: "",
    category: "",
    tag: "",
  });
  const [getSinglePost, { data: singlePost }] =
    useLazyQuery(FETCH_PER_POST_QUERY);

  const [loading, setLoading] = useState(false);
  // router
  const { postid } = useParams();
  // destructure
  const {
    title,
    body,
    imageurl,
    audiourl,
    videourl,
    youtubeurl,
    profilename,
    useremail,
    createdAt,
    category,
    tag,
  } = values;
  useMemo(() => {
    if (singlePost) {
      console.log(singlePost.getPost);
      setValues({
        ...values,
        //   _id: singlePost.getPost._id,
        title: singlePost.getPost.title,
        body: singlePost.getPost.body,
        imageurl:
          singlePost.getPost.imageurl != null
            ? singlePost.getPost.imageurl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.imageurl,
        audiourl:
          singlePost.getPost.audiourl != null
            ? singlePost.getPost.audiourl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.audiourl,
        videourl: singlePost.getPost.videourl,
        youtubeurl: singlePost.getPost.youtubeurl,
        profilename: singlePost.getPost.user.profilename,
        useremail: singlePost.getPost.user.email,
        createdAt: singlePost.getPost.createdAt,
        category:
          singlePost.getPost.category != null
            ? singlePost.getPost.category.name
            : "",
        tag: singlePost.getPost.tag != null ? singlePost.getPost.tag.name : "",
      });
    }
  }, [singlePost]);

  useEffect(() => {
    console.log(postid);
    getSinglePost({ variables: { postId: postid } });
  }, []);

  return (
    <div className="container">
      <div className="blog-single gray-bg">
        <div className="container">
          <div className="row align-items-start">
            <div className="col-lg-8 m-15px-tb">
              <article className="article">
                <div className="article-img">
                  <img src={imageurl} title="" alt="" />
                  <br></br>
                  {(youtubeurl != "" &&  youtubeurl != null) ? (
                    <div className="player-wrapper1">
                      <ReactPlayer
                        url={youtubeurl}
                        className="react-player"
                         width="100%"
                        height="100%"
                      />
                    </div>
                  ) : (
                    <hr></hr>
                  )}

                  {audiourl != "" && audiourl != null ? (
                    <div className="form-group">
                      <label className="font-weight-bold">Audio link</label>
                      <Player url={audiourl} />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="article-title">
                  {/* <h6><a href="#">Lifestyle</a></h6> */}
                  <h2>{title}</h2>
                  <div className="media">
                    <div className="avatar">
                      <img
                        src="https://bootdey.com/img/Content/avatar/avatar1.png"
                        title=""
                        alt=""
                      />
                    </div>
                    <div className="media-body">
                      <label>{profilename}</label>
                      <span>{createdAt}</span>
                    </div>
                  </div>
                </div>
                <div className="article-content">{body}</div>
                <div className="nav tag-cloud">
                  <a href="#">{category}</a>
                </div>
              </article>
              {/* <div className="contact-form article-comment">
                        <h4>Leave a Reply</h4>
                        <form id="contact-form" method="POST">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <input name="Name" id="name" placeholder="Name *" className="form-control" type="text" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <input name="Email" id="email" placeholder="Email *" className="form-control" type="email" />
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <textarea name="message" id="message" placeholder="Your message *" rows="4" className="form-control"></textarea>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="send">
                                        <button className="px-btn theme"><span>Submit</span> <i className="arrow"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> */}
            </div>
            <div className="col-lg-4 m-15px-tb blog-aside">
              <div className="widget widget-author">
                <div className="widget-title">
                  <h3>Author</h3>
                </div>
                <div className="widget-body">
                  <div className="media align-items-center">
                    <div className="avatar">
                      <img
                        src="https://bootdey.com/img/Content/avatar/avatar6.png"
                        title=""
                        alt=""
                      />
                    </div>
                    <div className="media-body">
                      <h6>
                        Hello, I'm<br></br> {profilename}{" "}
                      </h6>
                    </div>
                  </div>
                  {/* <p>I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores</p> */}
                </div>
              </div>

              {/* <div className="widget widget-post">
                        <div className="widget-title">
                            <h3>Trending Now</h3>
                        </div>
                        <div className="widget-body">

                        </div>
                    </div> */}

              {/* <div className="widget widget-latest-post">
                        <div className="widget-title">
                            <h3>Latest Post</h3>
                        </div>
                        <div className="widget-body">
                            <div className="latest-post-aside media">
                                <div className="lpa-left media-body">
                                    <div className="lpa-title">
                                        <h5><a href="#">Prevent 75% of visitors from google analytics</a></h5>
                                    </div>
                                    <div className="lpa-meta">
                                        <a className="name" href="#">
                                            Rachel Roth
                                        </a>
                                        <a className="date" href="#">
                                            26 FEB 2020
                                        </a>
                                    </div>
                                </div>
                                <div className="lpa-right">
                                    <a href="#">
                                        <img src="https://via.placeholder.com/400x200/FFB6C1/000000" title="" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div className="latest-post-aside media">
                                <div className="lpa-left media-body">
                                    <div className="lpa-title">
                                        <h5><a href="#">Prevent 75% of visitors from google analytics</a></h5>
                                    </div>
                                    <div className="lpa-meta">
                                        <a className="name" href="#">
                                            Rachel Roth
                                        </a>
                                        <a className="date" href="#">
                                            26 FEB 2020
                                        </a>
                                    </div>
                                </div>
                                <div className="lpa-right">
                                    <a href="#">
                                        <img src="https://via.placeholder.com/400x200/FFB6C1/000000" title="" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div className="latest-post-aside media">
                                <div className="lpa-left media-body">
                                    <div className="lpa-title">
                                        <h5><a href="#">Prevent 75% of visitors from google analytics</a></h5>
                                    </div>
                                    <div className="lpa-meta">
                                        <a className="name" href="#">
                                            Rachel Roth
                                        </a>
                                        <a className="date" href="#">
                                            26 FEB 2020
                                        </a>
                                    </div>
                                </div>
                                <div className="lpa-right">
                                    <a href="#">
                                        <img src="https://via.placeholder.com/400x200/FFB6C1/000000" title="" alt="" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> */}

              <div className="widget widget-tags">
                <div className="widget-title">
                  <h3>Latest Tags</h3>
                </div>
                <div className="widget-body">
                  <div className="nav tag-cloud">
                    <a href="#">{tag}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PostDetail;
