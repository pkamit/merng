import React, {
  useState,
  useContext,
  useEffect,
  fragment,
  createRef,
} from "react";
import { toast } from "react-toastify";
import { AuthContext } from "../../context/authContext";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Button from "react-bootstrap/Button";
//import omitDeep from 'omit-deep';
//import FileUpload from '../../components/FileUpload';
import FileUpload from "../../components/FileUpload";
import axios from "axios";
import { getCategories } from "../../functions/category";
import Form from "react-bootstrap/Form";
import { getTags } from "../../functions/tags";
import ImageForm from "../../components/forms/ImageForm";
import AudioForm from "../../components/forms/AudioForm";
import VideoForm from "../../components/forms/VideoForm";

import { POST_CREATE } from "../../util/mutation";

const initialState = {
  title: "",
  body: "",
  image: "https://via.placeholder.com/200x200.png?text=Post",
  file: "",
  youtube: "",
  imgfile: "",
  categories: [],
  tags: [],
  category: "",
  tag: ""
  
};

const Post = () => {
  const { state } = useContext(AuthContext);
  const [tags, setTags] = useState([]);
  const [values, setValues] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [fileinfo, setFileInfo] = useState([]);
  const [audiofileinfo, setAudioFileInfo] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedTag, setSelectedTag] = useState("");
  const rememberMeEl = React.useRef(null);


  const [postCreate] = useMutation(POST_CREATE);

  // destructure

  // useEffect(() => {
  //    loadTags();
  // }, []);
  useEffect(() => {
    loadCategories();
  }, []);
  const loadCategories = () => {
    getCategories().then((c) => setValues({ ...values, categories: c.data }));
    getTags().then((c) => setTags(c.data));

    //   getTags().then((tag) => setValues({ ...values, tags: tag.data }));
  };

  // destructure
  const { title, image, body, youtube, imgfile, categories } = values;

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    values.category = selectedCategory ? selectedCategory : values.category;
    // console.log(fileinfo);
    // console.log(selectedTag);
    // console.log(values);

    const formData = new FormData();
    //formData.append("title", values.title);
    // formData.append("file", values.file);
    // formData.append("title", values.title);
    // formData.append("body", values.body);

    formData.append("file", fileinfo);
    // reCreate new Object and set File Data into it
    axios
      .post(`${process.env.REACT_APP_API}/uploadimage`, formData, {
        headers: {
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",

          authtoken: state.user.token,
        },
      })
      .then((response) => {
        setLoading(false);
        var fileurl = response.data.fileUrl;
        let publish= 0;
        if(rememberMeEl.current.checked){
           publish = 1;
        }
        if (audiofileinfo != "") {
          const formDataForAudio = new FormData();
          //formData.append("title", values.title);
          // formData.append("file", values.file);
          // formData.append("title", values.title);
          // formData.append("body", values.body);

          formDataForAudio.append("file", audiofileinfo);
          axios
            .post(
              `${process.env.REACT_APP_API}/uploadaudio`,
              formDataForAudio,
              {
                headers: {
                  Accept: "application/json",
                  "Access-Control-Allow-Origin": "*",
                  authtoken: state.user.token,
                },
              }
            )
            .then((audioresponse) => {
              var audioFileUrl = audioresponse.data.fileUrl;
              const newValues = {
                title: values.title,
                body: values.body,
                imageurl: fileurl,
                audiourl: audioFileUrl,
                videourl: "",
                youtubeurl: "",
                likes: 0,
                comments: 0,
                category: values.category,
                tag: selectedTag,
                publish:publish
              };
              console.log("CLOUDINARY AUDIO UPLOAD", newValues);
              if (audioFileUrl != "") {
                postCreate({ variables: { input: newValues } });
                setLoading(false);
                // toast.success("Post Updated");
                window.alert(`"${values.title}" is created`);
            //    window.location.reload();
              } else {
                toast.error("Some issue with files", {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            });
        } else {
          const newValues = {
            title: values.title,
            body: values.body,
            imageurl: fileurl,
            audiourl: "",
            videourl: "",
            youtubeurl: values.youtube,
            likes: 0,
            comments: 0,
            category: values.category,
            tag: selectedTag,
            publish:publish
          };
          console.log("CLOUDINARY UPLOAD", newValues);
          if (fileurl != "") {
            postCreate({ variables: { input: newValues } });
            setLoading(false);
            // toast.success("Post Updated");
            window.alert(`"${values.title}" is created`);
           // window.location.reload();
          } else {
            toast.error("Some issue with files", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        }
        // setValues to parent component based on either it is
        // used htmlFor single/multiple upload
        // if (singleUpload) {
        //     // single upload
        //     const { image } = values;
        //     setValues({ ...values, image: response.data });
        // } else {
        //     const { images } = values;
        //     setValues({ ...values, images: [...images, response.data] });
        // }
      })
      .catch((error) => {
        setLoading(false);
        console.log("CLOUDINARY UPLOAD FAILED", error);
      });

    // ;
  };

  const handleChange = (e) => {
    e.preventDefault();
    setValues({ ...values, [e.target.name]: e.target.value });
   // console.log();
  };
  const handleOnUploadFile = (e) => {
    e.preventDefault();
    loadCategories();
    //console.log(e.target.files[0]);
    setValues({ ...values, file: e.target.files[0] });
    setFileInfo(e.target.files[0]);
  };

  const handleOnAudioUploadFile = (e) => {
    e.preventDefault();
    loadCategories();
    //console.log(e.target.files[0]);
    setValues({ ...values, audiofile: e.target.files[0] });
    setAudioFileInfo(e.target.files[0]);
  };
  const handleCatagoryChange = (e) => {
    e.preventDefault();
    console.log("CLICKED CATEGORY", e.target.value);
    setSelectedCategory(e.target.value);
    // setValues({ ...values, subs: [], category: e.target.value });
    // getCategorySubs(e.target.value).then((res) => {
    //   console.log("SUB OPTIONS ON CATGORY CLICK", res);
    //   setSubOptions(res.data);
    // });
    // setShowSub(true);
  };

  const handleTagChange = (e) => {
    e.preventDefault();
    console.log("CLICKED CATEGORY", e.target.value);
    setSelectedTag(e.target.value);
    // setValues({ ...values, subs: [], category: e.target.value });
    // getCategorySubs(e.target.value).then((res) => {
    //   console.log("SUB OPTIONS ON CATGORY CLICK", res);
    //   setSubOptions(res.data);
    // });
    // setShowSub(true);
  };
  const handleCheck = (e) => {};
  const createImageForm = () => (
    <ImageForm
      handleSubmit={handleSubmit}
      handleChange={handleChange}
      title={title}
      body={body}
      handleOnUploadFile={handleOnUploadFile}
      handleCatagoryChange={handleCatagoryChange}
      handleTagChange={handleTagChange}
      values={values}
      setValues={setValues}
      tags={tags}
      selectedCategory={selectedCategory}
      selectedTag={selectedTag}
      rememberMeEl={rememberMeEl}
    />
  );

  const createAudioForm = () => (
    <AudioForm
      handleSubmit={handleSubmit}
      handleChange={handleChange}
      title={title}
      body={body}
      handleOnUploadFile={handleOnUploadFile}
      handleOnAudioUploadFile={handleOnAudioUploadFile}
      handleCatagoryChange={handleCatagoryChange}
      handleTagChange={handleTagChange}
      values={values}
      setValues={setValues}
      tags={tags}
      selectedCategory={selectedCategory}
      selectedTag={selectedTag}
      rememberMeEl={rememberMeEl}
    />
  );

  const createVideoForm = () => (
    <VideoForm
      handleSubmit={handleSubmit}
      handleChange={handleChange}
      title={title}
      body={body}
      youtube={youtube}
      handleOnUploadFile={handleOnUploadFile}
      handleCatagoryChange={handleCatagoryChange}
      handleTagChange={handleTagChange}
      values={values}
      setValues={setValues}
      tags={tags}
      selectedCategory={selectedCategory}
      selectedTag={selectedTag}
      rememberMeEl={rememberMeEl}
    />
  );

  return (
    <div className="container p-5">
      {loading ? (
        <h4 className="text-danger">Loading...</h4>
      ) : (
        <h4>Create Post</h4>
      )}

      {/* <FileUpload
                values={values}
                loading={loading}
                setValues={setValues}
                setLoading={setLoading}
                singleUpload={true}
            /> */}

      <Tabs defaultActiveKey="image" id="uncontrolled-tab-example">
        <Tab eventKey="image" title="Image">
          <div className="row">
            <div className="col">{createImageForm()}</div>
          </div>
        </Tab>
        <Tab eventKey="audio" title="Audio">
          <div className="row">
            {/* <div className="col">{createAudioForm()}</div> */}
            <div className="col">{createAudioForm()}</div>
          </div>
        </Tab>
        <Tab eventKey="video" title="Youtube">
          <div className="row">
            <div className="col">{createVideoForm()}</div>
          </div>
        </Tab>
      </Tabs>
    </div>
  );
};

export default Post;
