import React, {
  useState,
  useContext,
  useEffect,
  fragment,
  createRef,
  useMemo,
} from "react";
import { toast } from "react-toastify";
import { AuthContext } from "../../context/authContext";
import { useQuery, useMutation, useLazyQuery } from "@apollo/react-hooks";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Button from "react-bootstrap/Button";
import { FETCH_PER_POST_QUERY } from "../../util/graphql";
import { POST_UPDATE } from "../../util/mutation";
//import omitDeep from 'omit-deep';
//import FileUpload from '../../components/FileUpload';
import FileUpload from "../../components/FileUpload";
import axios from "axios";
import { getCategories } from "../../functions/category";
import Form from "react-bootstrap/Form";
import { getTags } from "../../functions/tags";
import UpdateForm from "../../components/forms/UpdateForm";
import AudioForm from "../../components/forms/AudioForm";
import VideoForm from "../../components/forms/VideoForm";
import { useParams } from "react-router-dom";

import { POST_CREATE } from "../../util/mutation";

const initialState = {
  title: "",
  body: "",
  image: "https://via.placeholder.com/200x200.png?text=PostUpdateNew",
  file: "",
  imageurl: "",
  youtubeurl: "",
  imgfile: "",
  categories: [],
  tags: [],
  category: "",
  tag: "",
  category_id: "",
  tag_id: "",
  publish: 0,
};

const PostUpdateNew = () => {
  const { state } = useContext(AuthContext);
  const [tags, setTags] = useState([]);
  const [values, setValues] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [fileinfo, setFileInfo] = useState([]);
  const [filenew, setFileNewInfo] = useState([]);
  const [audiofileinfo, setAudioFileInfo] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedTag, setSelectedTag] = useState("");
  const rememberMeEl = React.useRef(null);

  const [postCreate] = useMutation(POST_CREATE);
  const [getSinglePost, { data: singlePost }] =
    useLazyQuery(FETCH_PER_POST_QUERY);
  const [postUpdate] = useMutation(POST_UPDATE);

  // router
  const { postid } = useParams();
  // destructure

  useMemo(() => {
    if (singlePost) {
      setValues({
        ...values,
        _id: singlePost.getPost._id,
        title: singlePost.getPost.title,
        body: singlePost.getPost.body,
        imageurl:
          singlePost.getPost.imageurl != null
            ? singlePost.getPost.imageurl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.imageurl,
        audiourl:
          singlePost.getPost.audiourl != null
            ? singlePost.getPost.audiourl.replace(
                /10.0.2.2:8000/gi,
                "localhost:8000"
              )
            : singlePost.getPost.audiourl,
        videourl: singlePost.getPost.videourl,
        youtubeurl: singlePost.getPost.youtubeurl,
        profilename: singlePost.getPost.user.profilename,
        useremail: singlePost.getPost.user.email,
        category_id:
          singlePost.getPost.category != null
            ? singlePost.getPost.category.id
            : "",
        category:
          singlePost.getPost.category != null
            ? singlePost.getPost.category.id
            : "",
        tag: singlePost.getPost.tag != null ? singlePost.getPost.tag.id : "",
        publish: singlePost.getPost.publish,
      });
    }
  }, [singlePost]);

  // useEffect(() => {
  //    loadTags();
  // }, []);

  useEffect(() => {
    loadCategories();
    getSinglePost({ variables: { postId: postid } });
  }, []);
  const loadCategories = () => {
    getCategories().then((c) => setValues({ ...values, categories: c.data }));
    getTags().then((c) => setTags(c.data));

    //   getTags().then((tag) => setValues({ ...values, tags: tag.data }));
  };

  // destructure
  //const { title, image, body, youtube, imgfile, categories } = values;
  const {
    title,
    body,
    imageurl,
    audiourl,
    videourl,
    youtubeurl,
    profilename,
    useremail,
    publish,
    category_id,
    tag,
    category,
    file
  } = values;
  JSON.stringify(values);
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    values.category = selectedCategory ? selectedCategory : values.category;
    // console.log(fileinfo);
    // console.log(selectedTag);
    // console.log(values);

    const formData = new FormData();
    //formData.append("title", values.title);
    // formData.append("file", values.file);
    // formData.append("title", values.title);
    // formData.append("body", values.body);

    formData.append("file", fileinfo);
    // reCreate new Object and set File Data into it
   // console.log(fileinfo);
    if (imageurl != "" && fileinfo == "" ) {
      console.log(values);   
      const newValues = {
        _id: values._id,
        title: values.title,
        body: values.body,
        category: selectedCategory != "" ? selectedCategory : values.category,
        tag: selectedTag != "" ? selectedTag : values.tag,
        publish: rememberMeEl.current.checked ? 1 : 0,
      };
       postUpdate({ variables: { input: newValues } });
       setLoading(false);
       console.log("CLOUDINARY Image UPLOAD", newValues);
       window.alert(`"${values.title}" is created`);

    } else {
      axios
        .post(`${process.env.REACT_APP_API}/uploadimage`, formData, {
          headers: {
            Accept: "application/json",
            "Access-Control-Allow-Origin": "*",

            authtoken: state.user.token,
          },
        })
        .then((response) => {
          setLoading(false);
          var fileurl = response.data.fileUrl;
          let publish = 0;
          if (rememberMeEl.current.checked) {
            publish = 1;
          }
          if (audiofileinfo != "") {
            const formDataForAudio = new FormData();
            //formData.append("title", values.title);
            // formData.append("file", values.file);
            // formData.append("title", values.title);
            // formData.append("body", values.body);

            formDataForAudio.append("file", audiofileinfo);
            axios
              .post(
                `${process.env.REACT_APP_API}/uploadaudio`,
                formDataForAudio,
                {
                  headers: {
                    Accept: "application/json",
                    "Access-Control-Allow-Origin": "*",
                    authtoken: state.user.token,
                  },
                }
              )
              .then((audioresponse) => {
                var audioFileUrl = audioresponse.data.fileUrl;
                const newValues = {
                  _id: values._id,
                  title: values.title,
                  body: values.body,
                  imageurl: fileurl,
                  audiourl: audioFileUrl,
                  videourl: "",
                  youtubeurl: "",
                  likes: 0,
                  comments: 0,
                  category: selectedCategory != "" ? selectedCategory : values.category,
                  tag: selectedTag != "" ? selectedTag : values.tag,
                  publish:  rememberMeEl.current.checked ? 1 : 0,
                };
                console.log("CLOUDINARY AUDIO UPLOAD", newValues);
                if (audioFileUrl != "") {
                  postUpdate({ variables: { input: newValues } });
                  setLoading(false);
                  // toast.success("Post Updated");
                  window.alert(`"${values.title}" is created`);
                  //    window.location.reload();
                } else {
                  toast.error("Some issue with files", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                }
              });
          } else {
            const newValues = {
              _id: values._id,
              title: values.title,
              body: values.body,
              imageurl: fileurl,
              audiourl: "",
              videourl: "",
              youtubeurl: values.youtube,
              likes: 0,
              comments: 0,
              category: selectedCategory != "" ? selectedCategory : values.category,
              tag: selectedTag != "" ? selectedTag : values.tag,
              publish:  rememberMeEl.current.checked ? 1 : 0,
            };
            console.log("CLOUDINARY UPLOAD", newValues);
            if (fileurl != "") {
              postUpdate({ variables: { input: newValues } });
              setLoading(false);
              // toast.success("Post Updated");
              window.alert(`"${values.title}" is created`);
              // window.location.reload();
            } else {
              toast.error("Some issue with files", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            }
          }
          // setValues to parent component based on either it is
          // used htmlFor single/multiple upload
          // if (singleUpload) {
          //     // single upload
          //     const { image } = values;
          //     setValues({ ...values, image: response.data });
          // } else {
          //     const { images } = values;
          //     setValues({ ...values, images: [...images, response.data] });
          // }
        })
        .catch((error) => {
          setLoading(false);
          console.log("CLOUDINARY UPLOAD FAILED", error);
        });
    }

    // ;
  };

  const handleChange = (e) => {
    e.preventDefault();
    setValues({ ...values, [e.target.name]: e.target.value });
    // console.log();
  };
  const handleOnUploadFile = (e) => {
    e.preventDefault();
    loadCategories();
    //console.log(e.target.files[0]);
    setValues({ ...values, file: e.target.files[0] });
    //setFileNewInfo(e.target.files[0]);
    setFileInfo(e.target.files[0]);
  };

  const handleOnAudioUploadFile = (e) => {
    e.preventDefault();
    loadCategories();
    //console.log(e.target.files[0]);
    setValues({ ...values, audiofile: e.target.files[0] });
    setAudioFileInfo(e.target.files[0]);
  };
  const handleCatagoryChange = (e) => {
    e.preventDefault();
    console.log("CLICKED CATEGORY", e.target.value);
    setSelectedCategory(e.target.value);
    // setValues({ ...values, subs: [], category: e.target.value });
    // getCategorySubs(e.target.value).then((res) => {
    //   console.log("SUB OPTIONS ON CATGORY CLICK", res);
    //   setSubOptions(res.data);
    // });
    // setShowSub(true);
  };

  const handleTagChange = (e) => {
    e.preventDefault();
    console.log("CLICKED CATEGORY", e.target.value);
    setSelectedTag(e.target.value);
    // setValues({ ...values, subs: [], category: e.target.value });
    // getCategorySubs(e.target.value).then((res) => {
    //   console.log("SUB OPTIONS ON CATGORY CLICK", res);
    //   setSubOptions(res.data);
    // });
    // setShowSub(true);
  };
  const handleCheck = (e) => {};
  const createUpdateForm = () => (
    <UpdateForm
      handleSubmit={handleSubmit}
      handleChange={handleChange}
      title={title}
      body={body}
      imageurl={imageurl}
      handleOnUploadFile={handleOnUploadFile}
      handleOnAudioUploadFile={handleOnAudioUploadFile}
      handleCatagoryChange={handleCatagoryChange}
      handleTagChange={handleTagChange}
      values={values}
      setValues={setValues}
      tags={tags}
      selectedCategory={selectedCategory}
      selectedTag={selectedTag}
      rememberMeEl={rememberMeEl}
    />
  );

  return (
    <div className="container p-5">
      {loading ? (
        <h4 className="text-danger">Loading...</h4>
      ) : (
        <h4>Update Post</h4>
      )}

      {/* <FileUpload
                  values={values}
                  loading={loading}
                  setValues={setValues}
                  setLoading={setLoading}
                  singleUpload={true}
              /> */}

      <Tabs defaultActiveKey="image" id="uncontrolled-tab-example">
        <Tab eventKey="image" title="Image">
          <div className="row">
            <div className="col">{createUpdateForm()}</div>
          </div>
        </Tab>
      </Tabs>
    </div>
  );
};

export default PostUpdateNew;
