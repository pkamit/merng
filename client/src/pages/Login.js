import React, { useState, useEffect, useContext } from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import {
    GoogleOutlined,
 
 } from "@ant-design/icons";
 import { auth, googleAuthProvider } from "../firebase";
 import { toast } from "react-toastify";
 import { createOrUpdateUser } from "../functions/auth";
 import {AuthContext} from '../context/authContext'

//import { LOGGED_IN_USER } from "../reducers/constant";
// import { LOGGED_IN_USER } from "../reducers/constant";
//import { AuthContext } from "./context/authContext";
const Login = ({ history }) =>{

  const {state , dispatch} = useContext(AuthContext);
  var retrievedObject = localStorage.getItem('addtoken');
  let user = JSON.parse(retrievedObject);

  // useEffect(() => {
  //   let intended = history.location.state;
  //   if (intended) {
  //     return;
  //   } else {
  //     if (user && user.token) history.push("/");
  //   }
  // }, [user, history]);
  const roleBasedRedirect = (res) => {
    // check if intended
    let intended = history.location.state;
    console.log("Intended" ,intended);
    if (intended) {
      history.push(intended.from);
    } else {
      console.log(res.data);
      if (res.data.role === "admin") {
      //  history.push("/admin/dashboard");
     // console.log(window.location.host);
     history.push("/");
     //   window.location.href = "http://"+window.location.host+"/";
      } else {
        history.push("/user/history");
      }
    }
  };
    const [loading, setLoading] = useState(true);

    const googleLogin = async () => {
        auth
          .signInWithPopup(googleAuthProvider)
          .then(async (result) => {
            const { user } = result;
            const idTokenResult = await user.getIdTokenResult();
            
            createOrUpdateUser(idTokenResult.claims.user_id)
              .then((res) => {
                res.data.token = idTokenResult.token;
                const token = localStorage.setItem("addtoken",  JSON.stringify(res.data));
                console.log(res.data.token);
                dispatch({
                  type: 'LOGGED_IN_USER',
                  payload: {
                    name: res.data.profilename,
                    email: res.data.email,
                    token: res.data.token,
                    url :  res.data.url,
                    role: res.data.role,
                    uuid: res.data.uid
                   },
                });
               // roleBasedRedirect(res, history);
             //  window.location.reload();
             // history.push("/register");
             roleBasedRedirect(res, history);
             })
              .catch((err) =>
                console.log(`error in posting via google login ${err.message}`)
              );
    
          //  history.push("/")
          })
          .catch((err) => {
            console.log(err);
            toast.error(err.message);
          });
      };
    return (
        <div className="container">
          <div className="row">
            <div className="col-md-3 offset-md-3">
              {loading ? (
                <h4 className="text-danger"> Loading ...</h4>
              ) : (
                <h4>Login</h4>
              )}
    
              {
              //loginForm()
              }
    
              <Button
                onClick={
                    googleLogin
                }
                type="danger"
                className="mb-3"
                block
                shape="round"
                icon={<GoogleOutlined />}
                size="large"
              >
                Login with Google
              </Button>

              <Link to="/forgot/password" className="float-right text-danger">
                Forgot Password
              </Link>
            </div>
          </div>
        </div>
      );

}

export default Login;