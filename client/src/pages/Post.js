import React, { useEffect, useState } from "react";
import AdminNav from "../components/nav/AdminNav";
import YouTubePlayer from 'youtube-player';
import { FETCH_PER_POST_QUERY } from '../util/graphql';
import { useQuery  } from '@apollo/react-hooks';
import ReactPlayer from "react-player";
import MultiPlayer from './Multiplayer'
import parse from 'html-react-parser';
const Post = ({ match }) => {
  

    let id  = match.params.id;
    const {
        data, error,   loading
      } = useQuery( FETCH_PER_POST_QUERY, {  variables: {
          
          postId: match.params.id
       
        }
     // , fetchPolicy :"cache-and-network"
      },
      { errorPolicy: 'all' }
        );


        if (loading || !data) return <div>loading</div>;
    //    console.log("error"+JSON.stringify(error));
  console.log("data" + data);
   const playAudio = () => {
      if(data.getPost.audiourl !=null){
      new Audio(data.getPost.audiourl).play();
      }
    }


  var postMedia;
  console.log(data.getPost.imageurl);
  if (data.getPost.imageurl != null) {
    data.getPost.imageurl= data.getPost.imageurl.replace(/10.0.2.2:8000/gi, "localhost:8000");

    postMedia =  <div ><img  width='100%' src={data.getPost.imageurl} /></div>
  } else if (data.getPost.youtubeurl != null){
    postMedia =	<div className='player-wrapper'> <ReactPlayer url={data.getPost.youtubeurl}       className='react-player'
    playing
    width='100%'
    height='100%' /> </div>
  }
   else if (data.getPost.audiourl != null){
    data.getPost.audiourl= data.getPost.audiourl.replace(/10.0.2.2:8000/gi, "localhost:8000");
  postMedia =        <MultiPlayer
  urls={[
    // 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3',
    // 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3',
    // 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3',
    data.getPost.audiourl
    
   
  ]}
/>
}else{
  postMedia = "No data available";
}
 const handleSubmit=(event)=> {
  alert('A name was submitted: ');
  event.preventDefault();
}
const handleChange=(event)=> {
  alert('A name was submitted: ');

}
  return (
    <div className="container-fluid" >
    <div className="row">
      <div className="col-md-2">
        <AdminNav />
      </div>

      <div className="col">
      <h4>Post detail page</h4>
      <div class="container" id="product-section">
  <div class="row">
   <div class="col-md-5">
    
  {postMedia}

   </div>
   <div class="col-md-7">
    <div>
   
 
 
    <form onSubmit={handleSubmit}>
  <div class="form-group">
    <label for="exampleInputTitle">Title</label>
    <input type="text" class="form-control" id="exampleInputTitle" aria-describedby="emailHelp" value={data.getPost.title} />
    
  </div>
  <div class="form-group">
    <label for="exampleInputDescription">Description</label>
   
    <textarea id="exampleInputDescription" class="form-control"
          defaultvalue={data.getPost.body}
          onChange={handleChange}
        />
  
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    <hr/>
    </div>
    <div>
   
    </div>

  
   </div>
  </div>
 </div>
      </div>
    </div>
  </div>
  );
}


export default Post;