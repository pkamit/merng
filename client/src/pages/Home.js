import React, { useState, useContext } from "react";
import ApolloClient from "apollo-boost";
import { gql } from "apollo-boost";
import { useQuery, useLazyQuery, useMutation } from "@apollo/react-hooks";
import { AuthContext } from "../context/authContext";
import { useHistory } from "react-router-dom";
import ActionButton from "antd/lib/modal/ActionButton";
import { List, Avatar } from "antd";
import { FETCH_POSTS_QUERY } from "../util/graphql";
import { POST_PUBLISH } from "../util/mutation";
import { toast } from "react-toastify";

const Home = () => {
  const [allpost, setAllPosts] = useState({});
  const rememberMeEl = React.useRef(null);
  const [checked, setChecked] = React.useState(false);
  const { data, loading, error, fetchMore } = useQuery(FETCH_POSTS_QUERY, {
    variables: {
      offset: 0,
      limit: 10,
    },
  });

  const [fetchPosts, { data: posts }] = useLazyQuery(FETCH_POSTS_QUERY);
  // access context
  const { state, dispatch } = useContext(AuthContext);
  // react router
  let history = useHistory();

  const updateUserName = () => {
    dispatch({
      type: "LOGOUT",
      payload: "",
    });
  };

  const [postPublish] = useMutation(POST_PUBLISH);

  const publishCtrl = (e) => {
    let publish = 0;
    var message = "UnPublished";

   
    if (e.target.dataset.id != null) {
      // if(!publish){
      //   publish = 1;
      //   message = "Published";
      // }
      if(e.target.dataset.status == "0"){
        publish = 1;
        message = "Published";
      }
       postPublish({
        variables: { postId: e.target.dataset.id, publish: publish },
      });
      toast.success(`${e.target.dataset.title} has been ${message}`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    setChecked(!checked);
  };

  if (loading) return <p className="p-5">Loading...</p>;
  let url = "";
  return (
    <div className="container">
      {/* <pre>Bad: {error.map(({ message }, i) => (
       <span key={i}>{message}</span>
     ))}
     </pre> */}
      {
        <List
          itemLayout="horizontal"
          dataSource={data.posts.edges}
          renderItem={(item) => (
            <List.Item
              actions={[
                <a key="list-loadmore-edit">
                  <input
                    type="checkbox"
                    name="publishCheck"
                    defaultChecked={item.node.publish}
                    data-status={item.node.publish}
                    onChange={publishCtrl}
                    data-id={item.node._id}
                    data-title={item.node.title}
                  />
                </a>,
                <a
                  key="list-loadmore-edit"
                  href={`/post/update/${item.node._id}`}
                >
                  edit
                </a>,
                <a key="list-loadmore-delete">Delete</a>,
              ]}
            >
              <List.Item.Meta
                className="pt-3"
                avatar={
                  <Avatar
                    src={item.node.user.url.replace(
                      /10.0.2.2:8000/gi,
                      "localhost:8000"
                    )}
                  />
                }
                title={<a href={`/post/detail/${item.node._id}`}>{item.node.title}</a>}
                description={
                  "By:-" +
                  item.node.user.profilename +
                  ", Email:-" +
                  item.node.user.email +
                  ", CreatedAt:-" +
                  item.node.createdAt
                }
              />

              {item.node.body}
            </List.Item>
          )}
        />
      }

      {/* <ul>
       {data.posts.edges.map(({ node }) => (
         <li key={node.id}>{node.title}</li>
       ))}
     </ul> */}

      {data.posts.edges.length < data.posts.totalCount ? (
        <button
          onClick={() => {
            let endCursor = data.posts.edges.length;
            let totalCount = data.posts.totalCount;
            console.log(endCursor + "==" + totalCount);

            fetchMore({
              variables: { offset: endCursor },
              updateQuery: (prevResult, { fetchMoreResult }) => {
                fetchMoreResult.posts.edges = [
                  ...prevResult.posts.edges,
                  ...fetchMoreResult.posts.edges,
                ];

                return fetchMoreResult;
              },
            });
          }}
        >
          more
        </button>
      ) : (
        ""
      )}
    </div>
  );
};

export default Home;
