//import React, { useContext , useEffect , useState} from 'react';
import React, { useState, useEffect } from "react";
import { useQuery  } from '@apollo/react-hooks';
import { Grid, Transition } from 'semantic-ui-react';
import { List, Avatar } from 'antd';
//import { AuthContext } from '../context/auth';
import PostCard from '../components/PostCard';
//import PostForm from '../components/PostForm';
import { FETCH_POSTS_QUERY } from '../util/graphql';
import gql from 'graphql-tag';
import { ApolloClient, InMemoryCache } from "@apollo/client";

function Home() {
  const {
    data, error,   loading, fetchMore
  } = useQuery( FETCH_POSTS_QUERY, {  variables: {
      
      offset: 0,
      limit: 10
    }
 // , fetchPolicy :"cache-and-network"
  },
  { errorPolicy: 'all' }
    );
 // const { user } = useContext(AuthContext);
//if (error) return <div>errors</div>;
 if (loading || !data) return <div>loading</div>;
 console.log("error"+JSON.stringify(error));
 if (fetchMore === undefined) return;
//   const [loadingpage, setLoadingpage] = useState(false);
//   let user= null;
//   useEffect(()=>{
//     var retrievedObject = localStorage.getItem('addtoken');
//      user = JSON.parse(retrievedObject);
//      if(!loadingpage){
//     //  window.location.reload();
//          setLoadingpage(true);

//      }
//   },
// [localStorage.getItem('addtoken')])
// let  endCursor  = data.posts.edges.length;
// let  totalCount  = data.posts.totalCount;
// console.log(endCursor +  totalCount);
  const handleRemove = async (id) => {
    let answer = window.confirm("Delete?");
    if (answer) {
         alert(id);
    }
  };
  return (
    // <Grid columns={3}>
    //   <Grid.Row className="page-title">
    //     <h1>Recent Posts</h1>
    //   </Grid.Row>
    //   <Grid.Row>
    //        {loading ? (
    //       <h1>Loading posts..</h1>
    //     ) : (
    //       <Transition.Group>
    //         {posts &&
    //           posts.map((post) => (
    //             <Grid.Column key={post.id} style={{ marginBottom: 20 }}>
    //               <PostCard post={post} />
    //             </Grid.Column>
    //           ))}
    //       </Transition.Group>
    //     )}
    //   </Grid.Row>
    // </Grid>
   <div>
     {/* <pre>Bad: {error.map(({ message }, i) => (
        <span key={i}>{message}</span>
      ))}
      </pre> */}
    { <List
    itemLayout="horizontal"
    dataSource={data.posts.edges}
    renderItem={item => (
      <List.Item
      actions={[<a key="list-loadmore-edit"  href={`/admin/post/${item.node.id}`}>edit</a>, <a key="list-loadmore-delete">Delete</a>]}
      >
        <List.Item.Meta
          avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
          title={<a href={`/post/${item.node.id}`} >{item.node.title}</a>}
          description={item.node.body}
        />
      </List.Item>
     )}
     
     /> }

{/* <ul>
        {data.posts.edges.map(({ node }) => (
          <li key={node.id}>{node.title}</li>
        ))}
      </ul> */}

      {data.posts.edges.length < data.posts.totalCount ?    <button
        onClick={() => {
           let  endCursor  = data.posts.edges.length;
           let  totalCount  = data.posts.totalCount;
           console.log(endCursor + "=="+ totalCount);
      
            fetchMore({
            variables: { offset: endCursor },
            updateQuery: (prevResult, { fetchMoreResult }) => {
          
              
              fetchMoreResult.posts.edges = [
                ...prevResult.posts.edges,
                ...fetchMoreResult.posts.edges
              ]; 
            
              return fetchMoreResult;
            }
          });
        }}
  
      >
        more
      </button>: "" }
     </div>
    
   
     
  );
}

export default Home;