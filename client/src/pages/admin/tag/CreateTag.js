import React, { useState, useEffect , useContext} from "react";
import CategoryForm from "../../../components/forms/CategoryForm";
import { toast } from "react-toastify";
import {
  createTag,
  getTags,
  removeCategory,
} from "../../../functions/tags";
import { AuthContext } from '../../../context/authContext';
const CategoryTag = () => {
  const [name, setName] = useState("");
  const [slug, setSlug] = useState("");
  const [tags, setTags] = useState([]);
  const [loading, setLoading] = useState(false);
  const { state, dispatch } = useContext(AuthContext);


  const { user } = state;
  useEffect(() => {
    loadTags();
    
  }, []);
  const loadTags = () => {
  getTags().then((c) => setTags(c.data))
  console.log(JSON.stringify(tags));
};

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    createTag({ name ,slug } ,  user.token)
      .then((res) => {
        setLoading(false);
        toast.success(`"${res.data.name}" is created`);
        setName("");
        setSlug("");
        loadTags();
      })
      .catch((err) => {
        setLoading(false);
        if (err.response.status == 400) toast.error(err.response.data);
      });
  };
  return (

    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <CategoryForm
            handleSubmit={handleSubmit}
            name={name}
            setName={setName}
            slug={slug}
            setSlug={setSlug}
          />
           {tags.map((c) => (
                      <div key={c._id} className="alert alert-secondary">
                      {c.name}
                      {/* <span
                        className="btn btn-sm float-right"
                        onClick={() => handleRemove(c.slug)}
                      >
                        <DeleteOutlined className="text-danger" />
                      </span> */}
                      {/* <Link to={`/admin/category/${c.slug}`}>
                        <span className="btn btn-sm float-right">
                          <EditOutlined className="text-warning" />
                        </span>
                      </Link> */}
                    </div>
               ))}
        </div>
      </div>
    </div>
  );
};

export default CategoryTag;
