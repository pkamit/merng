import React, { useState, useEffect , useContext} from "react";
import CategoryForm from "../../../components/forms/CategoryForm";
import { toast } from "react-toastify";
import {
  createCategory,
  getCategories,
  removeCategory,
} from "../../../functions/category";
import { AuthContext } from '../../../context/authContext';
const CategoryCreate = () => {
  const [name, setName] = useState("");
  const [slug, setSlug] = useState("");
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);
  const { state, dispatch } = useContext(AuthContext);


  const { user } = state;
  useEffect(() => {
    loadCategories();
    
  }, []);
  const loadCategories = () => {
  getCategories().then((c) => setCategories(c.data))
  console.log(JSON.stringify(categories));
};

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    createCategory({ name ,slug } ,  user.token)
      .then((res) => {
        setLoading(false);
        toast.success(`"${res.data.name}" is created`);
        setName("");
        setSlug("");
        loadCategories();
      })
      .catch((err) => {
        setLoading(false);
        if (err.response.status == 400) toast.error(err.response.data);
      });
  };
  return (

    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <CategoryForm
            handleSubmit={handleSubmit}
            name={name}
            setName={setName}
            slug={slug}
            setSlug={setSlug}
          />
           {categories.map((c) => (
                      <div key={c._id} className="alert alert-secondary">
                      {c.name}
                      {/* <span
                        className="btn btn-sm float-right"
                        onClick={() => handleRemove(c.slug)}
                      >
                        <DeleteOutlined className="text-danger" />
                      </span> */}
                      {/* <Link to={`/admin/category/${c.slug}`}>
                        <span className="btn btn-sm float-right">
                          <EditOutlined className="text-warning" />
                        </span>
                      </Link> */}
                    </div>
               ))}
        </div>
      </div>
    </div>
  );
};

export default CategoryCreate;
