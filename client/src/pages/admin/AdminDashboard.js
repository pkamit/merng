import React from "react";
import AdminNav from "../../components/nav/AdminNav";
import Home from "../Home";


const AdminDashboard = () => {
 

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-2">
          <AdminNav />
        </div>

        <div className="col">
        <h4>Admin dashboard</h4>
          <Home />
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
