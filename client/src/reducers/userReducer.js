import * as actionType from './constant'
export const userReducer = (state = null , action)=>{
    switch (action.type) {
        case actionType.LOGGED_IN_USER:
            return action.payload;
        case actionType.LOGOUT:
            return action.payload;
        default:
            return state;
    }
}