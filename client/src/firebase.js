import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCBUc6OcjHx7cKvvmZhAim5bdTPPa5FbXk",
    authDomain: "add-app-test.firebaseapp.com",
    databaseURL: "https://add-app-test.firebaseio.com",
    projectId: "add-app-test",
    storageBucket: "add-app-test.appspot.com",
    messagingSenderId: "225500471028",
    appId: "1:225500471028:android:f926b8210012f7e4a7c8a1",
  };

  firebase.initializeApp(firebaseConfig);
  

  export const auth = firebase.auth();
  export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();